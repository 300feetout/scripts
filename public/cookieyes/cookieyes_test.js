

document.write('<script id="cookieyes" type="text/javascript" src="https://cdn-cookieyes.com/client_data/44633aa25740dba1e67bb925/script.js"></script>');

(function() {
	function delete_cookie( name, path, domain ) {
		if (get_cookie( name )) {
			document.cookie = name + "=" +
			((path) ? ";path="+path:"")+
			((domain)?";domain="+domain:"") +
			";expires=Thu, 01 Jan 1970 00:00:01 GMT"
		}
	}

	function get_cookie(name){
		return document.cookie.split(';').some(c => {
			return c.trim().startsWith(name + '=')
		})
	}

	function clearCookies() {
		setTimeout(() => {
			window.cookieyes._ckyStore._categories.map((cat) => {
				if (!window.cookieyes._ckyIsCategoryToBeBlocked(cat.slug)) {
					return
				}

				cat.cookies.map((cookie) => {
					if (!cookie.isNecessary) {
						console.log('delete_cookie', cookie)
						delete_cookie(cookie.cookieID, '/', cookie.domain)
					}
				})
			})
		}, 500)
	}

	document.addEventListener('click', (e) => {
		const target = e.target
		const dataset = target.dataset
		// Clear cookies if any of the controls that could/would clear cookies is clicked.
		if (dataset.ckyTag == 'detail-save-button') {
			clearCookies()
		} else if (dataset.ckyTag == 'detail-reject-button') {
			clearCookies()
		} else if (dataset.ckyTag == 'reject-button') {
			clearCookies()
		} else if (dataset.ckyTag == 'donotsell-button') {
			
		}
	})

	// const script = document.createElement('script')
	// script.src = 'https://cdn-cookieyes.com/client_data/44633aa25740dba1e67bb925/script.js'
	// const head = document.getElementsByTagName('head')[0]
	// head.prepend(script)

})();