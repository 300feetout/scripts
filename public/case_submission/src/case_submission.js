import 'idempotent-babel-polyfill'

import '@algolia/autocomplete-theme-classic'

import algoliaInstance from './algolia_instance.js'

require('./css/case_submission.styl');

try {
	(function() {
		let initialized = false
		let modalBackdrop
		let modal
		let modalUrl



		function sendMixpanelEvent(message, data) {
			if (typeof mixpanel == 'object' && mixpanel.track) {
				mixpanel.track(message, data)
			}
		}
		
		function mixpanelEvent(message, data = {}) {
			if (window.user) {
				data.user = window?.user?.detail
			}
			sendMixpanelEvent(message, data)
		}
		
		window.noButtonClick = function(e, message) {
			mixpanelEvent(message)
		}

		window.closeModal = function closeModal() {
			modalBackdrop.removeEventListener('click', closeModal)
			modal.addEventListener('click', onModalClicked)


			modalBackdrop.remove()
			modal.remove()
		}

		function onModalClicked(e) {
			
			const target = e.target
			if (target.nodeName == 'A') {
				if (target.classList.contains('no_solved')) {
					mixpanelEvent('Diversion - Unsolved', {
						'url': modalUrl
					})
					closeModal()
					openModal(null, `<div class="modal_no_content">
						<button onclick="closeModal()" class="close_modal">X</button>
						<h2>More options</h2>
						<a onclick="noButtonClick(event, 'Diversion - Ask the community')" class="button transparent black" href="/customers/s/">Ask the community</a><br />
						<a onClick="noButtonClick(event, 'Diversion - Create a case')" class="button transparent black" href="/customers/s/contactsupport">Create a case</a><br />
						<a onClick="noButtonClick(event, 'Diversion - Submit an idea')" class="button transparent black" href="/customers/s/idea-create-edit">Submit an idea</a>
					</div>`, '300px', '300px')
				} else {
					mixpanelEvent('Diversion - Solved', {
						'url': modalUrl
					})
					closeModal()
				}
			}
		}

		function openModal(url, content = null, width='800px', height='500px') {
			mixpanelEvent('Diversion - Open modal', {
				'url': url
			})

			modal = document.createElement('div')
			modal.classList.add('modal')
			modal.style.width = width
			modal.style.height = height
			if (url) {
				modalUrl = url

				if (modalUrl.indexOf('?') > -1 && modalUrl.indexOf('communityEmbed') == -1) {
					modalUrl += '&communityEmbed=1'
				} else if (modalUrl.indexOf('?') == -1 && modalUrl.indexOf('communityEmbed') == -1) {
					modalUrl += '?communityEmbed=1'
				}

				let useLocal = false
				if (window.location.search.indexOf('use_local_script=1') > -1) {
					useLocal = true
				}

				modalUrl += (useLocal ? '&use_local_script=1' : '')
			}

			let modalContent = `<div class="modal_content">`

			if (content) {
				modalContent += `${content}
				</div>`
			} else {
				modalContent += '<button onclick="closeModal()" class="close_modal iframe">X</button>'
				modalContent += `<iframe src="${modalUrl}"></iframe>
					<div class="solve_bar">Does this solve your issue? &nbsp; <a href="/" class="button solid black yes_solved">Yes</a> &nbsp; <a class="button transparent black no_solved">No</a></div>
				</div>`
			}

			modal.innerHTML = modalContent
			document.body.appendChild(modal)

			modalBackdrop = document.createElement('div')
			modalBackdrop.classList.add('modal_backdrop')
			document.body.appendChild(modalBackdrop)

			modalBackdrop.addEventListener('click', closeModal)
			modal.addEventListener('click', onModalClicked)

			return modal
		}

		async function CaseSubmissionComponent(CaseSubmissionWrapper, config) {
			const searchContainer = CaseSubmissionWrapper.querySelector(config.searchContainerSelector)
			const hitsContainer = CaseSubmissionWrapper.querySelector(config.hitsContainerSelector)
			const moreButton = CaseSubmissionWrapper.querySelector(config.moreButtonSelector)
			const hideWhenNoResults = CaseSubmissionWrapper.querySelectorAll(config.hideWhenNoResultsSelector)

			const {searchInstance, updateConfigure} = algoliaInstance({searchContainer, hitsContainer, moreButton, hideWhenNoResults})

			const searchInput = CaseSubmissionWrapper.querySelector(config.searchInputSelector)

			let pageSize = 8
			let page = 1

			document.querySelector('[data-skip]').addEventListener('click', (e) => {
				mixpanelEvent('Diversion - Skip')
			})
			document.querySelector('[data-back]').addEventListener('click', (e) => {
				mixpanelEvent('Diversion - Back')
			})

			hitsContainer.addEventListener('click', (e) => {
				e.preventDefault()
				e.stopPropagation()

				let target = e.target
				if (e.target.nodeName == 'H3') {
					target = target.parentElement
				}

				const url = target.href

				openModal(url)
			})

			moreButton.addEventListener('click', (e) => {
				e.preventDefault()
				page++
				updateConfigure({
					hitsPerPage: pageSize * page
				})
				// searchInstance.refresh()
			})

			this.api = {
				focusSearchInput: () => {
					searchInput.focus()
				}
			}

			if (config.startFocused) {
				this.api.focusSearchInput()
			}

			CaseSubmissionWrapper.style.opacity = 1

			return this.api
		}

		const CaseSubmissionComponentInit = async function(CaseSubmissionWrapperSelector, config) {
			if (!initialized) {
				initialized = true
				return new Promise((resolve, reject) => {
					const awaitDom = setInterval(() => {
						const CaseSubmissionWrapper = document.querySelector(CaseSubmissionWrapperSelector)
						if (CaseSubmissionWrapper && CaseSubmissionWrapper.initialized) {
							clearInterval(awaitDom)
							return
						}
						if (CaseSubmissionWrapper) {
							CaseSubmissionWrapper.initialized = true
							clearInterval(awaitDom)
							resolve(new CaseSubmissionComponent(CaseSubmissionWrapper, config))
						}
					}, 200)
				})
			}
		}

		// We need to re-run the init function when navigating back to the page.
		window.addEventListener('load', () => {
			if (window.location.pathname != '/customers/s/contact-support-diversion') {
				initialized = false
			} else {
				window.CaseSubmissionComponentInit()
			}
		})

		window.CaseSubmissionComponentInit = () => {
			CaseSubmissionComponentInit('.case_submission_wrapper', {
				searchWrapperSelector: '.case_submission_search_wrapper',
				searchInputSelector: 'input.ais-SearchBox-input',
				searchContainerSelector: '.case_submission_search_container',
				hitsContainerSelector: '.case_submission_hits_container',
				moreButtonSelector: '.case_submission_more_button',
				hideWhenNoResultsSelector: '.hide_when_no_results',
				startFocused: true
			})
		}

	})()
} catch (e) {
	console.log('IIFE error', e)
}