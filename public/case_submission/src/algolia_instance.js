import instantsearch from 'instantsearch.js'
import algoliasearch from 'algoliasearch/lite'

import extractUrls from 'extract-urls'
import truncate from 'truncate'

import {searchBox, hits, index, configure} from 'instantsearch.js/es/widgets'
import {connectConfigure} from 'instantsearch.js/es/connectors'

const baseCommunityUrl = 'https://community.thoughtspot.com/customers/s/'

function getRenderedHit(hit) {
	let target="_blank"
	let baseUrl = 'https://community.thoughtspot.com/'
	if (window.location.hostname == 'thoughtspot--preprod.sandbox.my.site.com') {
		baseUrl = 'https://thoughtspot--preprod.sandbox.my.site.com/'
	}
	let template = ''

	// Community types
	if (hit.object_type == 'FeedItemTextPost') {
		const communityUrl = baseUrl + 'customers/s/feed/'
		template = `<a class="result" href="${communityUrl}${hit.id}" target="${target}">
						<h3>${hit.title || hit.parent_name}</h3>
					</a>`
	} else if (hit.object_type == 'FeedItemQuestionPost') {
		const communityUrl = baseUrl + 'customers/s/question/'
		template = `<a class="result" href="${communityUrl}${hit.id}" target="${target}">
						<h3><span class="tag first">Discussions</span>${hit.title || hit.body}</h3>
					</a>`
	} else if (hit.object_type == 'FeedItemLead') {
		const communityUrl = `${baseUrl}customers/s/lead/`
		template = `<a class="result" href="${communityUrl}${hit.parent_id}" target="${target}">
						<h3><span class="tag first">lead: ${hit.parent_name}</span> ${hit.body}</h3>
					</a>`
	} else if (hit.object_type == 'Lead') {
		const communityUrl = `${baseUrl}customers/s/lead/`
		template = `<a class="result lead" href="${communityUrl}${hit.id}" target="${target}">
						<img src="${baseUrl}customers${hit.photo_url}" />
						<h3>${hit.name}</h3>
						<p class="last_activity_date"><span>last activity:</span> ${hit.last_activity_date}</p>
						<p class="email"><span>email:</span> ${hit.email}</p>
						<p><span>phone:</span> ${hit.phone}</span></p>
						<p><span>industry:</span> ${hit.industry}</span></p>
						<p><span>location:</span> ${hit.country}${hit.state ? ' , ' + hit.state : ''}</span></p>
					</a>`
	} else if (hit.object_type == 'KnowledgeArticle') {
		const communityUrl = `${baseUrl}customers/s/article/`
		template = `<a class="result" href="${communityUrl}${hit.url_name}" target="${target}">
						<h3><span class="tag first">Article</span> ${hit.title}</h3>
					</a>`
	} else if (hit.object_type == 'Group') {
		const communityUrl = `${baseUrl}customers/s/group/`
		template = `<a class="result" href="${communityUrl}${hit.id}" target="${target}">
						<h3><span class="tag first">Group</span>${hit.name}</h3>
					</a>`
	} else if (hit.object_type == 'Case') {
		const communityUrl = `${baseUrl}customers/s/case/`
		template = `<a class="result" href="${communityUrl}${hit.id}" target="${target}">
						<h3><span class="tag first">Case</span> ${hit.subject}</h3>
					</a>`
	} else if (hit.object_type == 'CaseComment') {
		const communityUrl = `${baseUrl}customers/s/case/`
		template = `<a class="result" href="${communityUrl}${hit.parent_id}" target="${target}">
						<h3><span class="tag first">Case Comment</span> ${hit.parent_subject}</h3>
					</a>`
	} else if (hit.object_type == 'File') {
		const communityUrl = `${baseUrl}customers/s/contentdocument/`
		let preview = ''
		if (['PNG', 'SVG', 'JPG', 'JPEG', 'GIF'].indexOf(hit.file_type) > -1) {
			preview = `<img src="${baseUrl}customers/sfc/servlet.shepherd/version/renditionDownload?rendition=ORIGINAL_Png&versionId=${hit.latest_published_version_id}&operationContext=CHATTER" />`
		}
		template = `<a class="result file" href="${communityUrl}${hit.id}" target="${target}">
						<h3><span class="tag first">${hit.file_type}</span> ${hit.title}</h3>
						${preview}
					</a>`
	} else if (hit.object_type == 'IdeaComment') {
		const communityUrl = `${baseUrl}s/idea/${hit.idea_id}/detail`
		template = `<a class="result" href="${communityUrl}" target="${target}">
						<h3><span class="tag first">Idea Comment</span> ${truncate(striptags(hit.comment_body), 240)}</h3>
					</a>`
	} else if (hit.object_type == 'Idea') {
		const communityUrl = `${baseUrl}s/idea/${hit.id}/detail`
		template = `<a class="result" href="${communityUrl}" target="${target}">
						<h3><span class="tag first">Idea</span> ${hit.title}</h3>
					</a>`
	}

	// Docs types
	if (hit.importedFrom == 'docs') {
		let url = hit.url.replace('https://docs.thoughtspot.com/', 'https://docs.thoughtspot.com/ghyer95y3/')
		if (url.indexOf('?') > -1) {
			url += '&communityEmbed=1'
		} else {
			url += '?communityEmbed=1'
		}
		return `<a class="result" href="${url}" target="${target}">
			<h3><span class="tag first">${hit.version}</span> ${hit.title}</h3>
		</a>`
	}

	// Help types
	if (hit.importedFrom == 'help') {
		let url = hit.url
		if (url.indexOf('?') > -1) {
			url += '&communityEmbed=1'
		} else {
			url += '?communityEmbed=1'
		}
		return `<a class="result" href="${url}" target="${target}">
			<h3><span class="tag first">${hit.version}</span> ${hit.title}</h3>
		</a>`
	}

	return template
}

export default function({searchContainer, hitsContainer, moreButton, hideWhenNoResults}) {
	const searchInstance = instantsearch({
		indexName: 'Case Submission',
		searchClient: algoliasearch('FFSASUS8DI', '8e22d8958711f962962f736908615bb3'),
		// searchFunction(helper) {
		// 	if (helper.state.query) {
		// 		helper.search()
		// 	}
		// }
	});

	let updateConfigure = null

	const renderConfigure = (renderOptions, isFirstRender) => {
		const {refine} = renderOptions

		if (isFirstRender) {
			updateConfigure = (searchParameters) => {
				refine(searchParameters)
			}
		}
	}

	const customConfigure = connectConfigure(
		renderConfigure
	)

	const onResults = (results) => {
		if (results) {
			[...hideWhenNoResults].map((elem) => {
				elem.style.display = ''
			})
			moreButton.style.display = ''
		} else {
			[...hideWhenNoResults].map((elem) => {
				elem.style.display = 'none'
			})
			moreButton.style.display = 'none'
		}
	}

	let widgets = [
		customConfigure({
			searchParameters: {
				hitsPerPage: 8,
			},
		}),
		hits({
			container: hitsContainer,
			templates: {
				empty: () => {
					onResults(false)
					return `<p>No results found</p>`
				},
				item: (hit) => {
					const template = getRenderedHit(hit)
					onResults(true)
					// let template = ''
					// let target = ''
					// let linkText = hit.title || hit.parent_name || hit.subject || hit.body || ''

					// console.log('hit', hit)

					// if (hit.object_type == 'KnowledgeArticle') {
					// 	hit.url = extractUrls(hit.url)[0]
					// }

					// if (hit.object_type == 'Case') {
					// 	hit.url = `${baseCommunityUrl}case/${hit.id}`
					// }

					// if (hit.object_type == 'CaseComment') {
					// 	hit.url = `${baseCommunityUrl}case/${hit.parent_id}`
					// 	linkText = hit.parent_subject
					// }

					// if (hit.object_type == 'FeedItemTextPost') {
					// 	hit.url = `${baseCommunityUrl}feed/${hit.id}`
					// }

					// if (hit.object_type == 'FeedItemQuestionPost') {
					// 	hit.url = `${baseCommunityUrl}question/${hit.id}`
					// }

					// template = `<a class="result" href="${hit.url}" target="${target}">
					// 				${linkText}
					// 			</a>`

					return template
				}
			}
		}),
		searchBox({
			container: searchContainer,
			placeholder: 'Type in keywords related to your issue'
		})
	]

	searchInstance.addWidgets(widgets)

	searchInstance.start()

	return {searchInstance, updateConfigure}
}