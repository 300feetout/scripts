if (window.location.search.indexOf('use_local_script=1') > -1) {
    var script = document.createElement('script');
    script.src = 'https://tsunav.ngrok.io/public/tsu_nav/dist/tsu_nav.bundle.js';
    document.head.appendChild(script);
} else {
    var script = document.createElement('script');
    script.src = 'https://scripts.thoughtspot.com/tsu_nav.bundle.js';
    document.head.appendChild(script);
}