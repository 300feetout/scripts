import fonts from './tsu_fonts.sass'
import css from './tsu_nav.sass'

function init(rootElement, config) {

	if (!rootElement.shadowRoot) {
		rootElement.dataset.initialized = 1

		const shadowRoot = rootElement.attachShadow({mode: 'open'})
		const shadowRootElement = document.createElement('div')
		shadowRootElement.style.width = '100%'
		shadowRoot.appendChild(shadowRootElement)

		function renderTopLevelItem(item) {

			const secondLevelSubnav = item.subnav ? renderSecondLevelSubnav(item.subnav) : ''

			let subnav = ''
			if (item?.subnav) {
				subnav = `<div class="subnav">
						<div class="mouse_bridge"></div>
						<div class="subnav_inner ${secondLevelSubnav ? 'has_second_level' : ''}">
							<div class="arrow"></div>
							<ul class="first_level_subnav">
								${renderSubnav(item.subnav)}
							</ul>
							${secondLevelSubnav}
						</div>
					</div>
				`
			}

			if (item.link) {
				return `
					<li class="top_level_li ${item?.subnav ? 'has_subnav' : ''}">
						<a href="${item.link}" class="top_level">
							<span>${item.text}</span>
							<span class="desktop_only" aria-hidden="true" style="font-weight:bold;opacity:0">${item.text}</span>
						</a>
						${subnav}
					</li>
				`
			} else {
				return `
					<li class="top_level_li ${item?.subnav ? 'has_subnav' : ''}">
						<button class="top_level">
							<span>${item.text}</span>
							<span class="desktop_only" aria-hidden="true" style="font-weight:bold;opacity:0">${item.text}</span>
						</button>
						${subnav}
					</li>
				`
			}
		}

		function renderSubnav(subnav) {
			return subnav.map((item) => {
				return `
					<li class="${item?.desktopOnly || (!item.link && !item.text) ? 'desktop_only' : ''}">
						<a href="${item.link}">
							${item.text}
						</a>
					</li>
				`
			}).join('')
		}

		function renderSecondLevelSubnav(subnav) {
			return subnav.map((item) => {
				if (item?.subnav) {
					return `
						<ul class="second_level_subnav">
							${renderSubnav(item.subnav)}
						</ul>
					`
				}
				return ''
			}).join('')
		}

		function renderNav(navConfig) {
			return navConfig.map((item) => {
				if (item.subnav) {
					return `
						${renderTopLevelItem(item)}
					`
				} else {
					return renderTopLevelItem(item)
				}
			}).join('')
		}

		shadowRootElement.innerHTML = `
			<style>${fonts}</style>
			<style>${css}</style>
			<div class="nav_wrapper" style="width: ${config.width}">
				<ul>
					${renderNav(config.links)}
				</ul>

				<button class="main_menu_toggle mobile_only" title="Toggle main menu">
					<span class="lines"></span>
					<span class="x"></span>
					<span class="screenreader_only">Toggle main menu</span>
				</button>
			</div>
			
		`
		const fontStyle = document.createElement('style')
		fontStyle.innerHTML = fonts
		document.body.appendChild(fontStyle)


		function initListeners() {
			const topLevelLinks = shadowRootElement.querySelectorAll('.top_level_li')

			if (window.innerWidth < 768) {
				topLevelLinks.forEach((link) => {
					link.addEventListener('click', (e) => {
						const subnavWrapper = e.currentTarget.querySelector('.subnav')
						if (subnavWrapper) {
							subnavWrapper.classList.toggle('active')
							e.currentTarget.classList.toggle('active')
						}
					})
				})				
			} else {
				topLevelLinks.forEach((link) => {
					link.addEventListener('mouseenter', (e) => {
						const subnav = e.target.querySelector('.subnav')
						if (subnav) {
							subnav.classList.add('show')
						}
					})
					link.addEventListener('mouseleave', (e) => {
						const subnav = e.target.querySelector('.subnav')
						if (subnav) {
							subnav.classList.remove('show')
						}
					})
				})
			}

			const mainMenuToggle = shadowRootElement.querySelector('.main_menu_toggle')
			mainMenuToggle.addEventListener('click', (e) => {
				shadowRootElement.querySelector('ul').classList.toggle('active')
				mainMenuToggle.classList.toggle('active')
			})
		}

		if (document.readyState === 'complete') {
			initListeners();
		} else {
			window.addEventListener('load', () => {
				initListeners();
			})
		}
	}
}

const awaitNavPrereqs = setInterval(() => {
	try {
		const config = JSON.parse(document.querySelector('[data-tsu-nav]').innerHTML);
		const rootElement = document.querySelector(config.container);
		if (rootElement && config) {
			clearInterval(awaitNavPrereqs);
			init(rootElement, config);
		}
	} catch (e) {
	}
}, 100)
