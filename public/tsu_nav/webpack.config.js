const path = require('path');
const CssMinimizerPlugin = require('css-minimizer-webpack-plugin');

module.exports = {
	entry: {
		tsu_nav: './src/tsu_nav.js',
		tsu_load: './src/tsu_load.js',
	},
	output: {
		filename: '[name].bundle.js',
		path: path.resolve(__dirname, 'dist'),
	},
	watch: true,
	mode: 'development',
	module: {
		rules: [
			{
				test: /\.s[ac]ss$/i,
				use: [
					// 'style-loader', // Creates `style` nodes from JS strings
					{
						loader: 'css-loader',
						options: {
							exportType: 'string', // Export as a string
						},
					},
					'sass-loader'   // Compiles Sass to CSS
				],
			},
		],
	},
	optimization: {
		minimizer: [
			'...', // Inclde default minimizers (e.g., for JS)
			new CssMinimizerPlugin(), // Minifies the CSS
		],
	},
};
