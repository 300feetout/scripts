const webpack = require('webpack')
const path = require('path')
const glob = require('glob')
const TerserPlugin = require("terser-webpack-plugin");

function webpackConfig(isProduction) {

	const config = {
		devtool: "source-map",
		mode: 'production',
		node: {
			global: false
		},
		resolve: {
			fallback: {
				"fs": false,
				"util": false,
				"path": false,
				"crypto": false,
				"zlib": false,
				"stream": false,
				"buffer": false,
				"https": false,
				"http": false,
				"url": false,
				"querystring": false,
				"os": false,
				"esbuild": false,
				"assert": false,
				"constants": false,
				"child_process": false,
				"worker_threads": false,
				"inspector": false,
				"@swc/core": false,
				"vm": false
			},
		},
		module: {
			rules: [
				{
					test: /\.m?js$/,
					exclude: /(node_modules|bower_components)/,
					use: {
						loader: 'babel-loader',
						options: {
							presets: ['@babel/preset-env'],
							sourceMap: !isProduction
						}
					}
				},
				{
					test: /\.css$/,
					use: [
						{
							loader: "style-loader" // creates style nodes from JS strings
						},
						{
							loader: "css-loader", // translates CSS into CommonJS
							options: {
								sourceMap: !isProduction
							}
						}
					]
				},
				{
					test: /\.styl$/,
					use: [
						{
							loader: "style-loader" // creates style nodes from JS strings
						},
						{
							loader: "css-loader" // translates CSS into CommonJS
						},
						{
							loader: "stylus-loader", // compiles Stylus to CSS
							options: {
								stylusOptions: {
									use: [
										require('nib')(),
										require('axis')(),
										require('rupture')(),
										require('jeet')()
									]
								},
								sourceMap: !isProduction
							}
						},
					]
				},
			]
		},
		plugins: [
			new webpack.DefinePlugin({
				global: 'window' // Placeholder for global used in any node_modules
			})
		],
		optimization: {
			minimize: isProduction ? true : false,
			minimizer: [new TerserPlugin({
				terserOptions: {
					mangle: {
						properties: {
							// Specify properties options
							regex: /apiKey|secureApiKey/ // Mangle all properties that involve apiKeys
						},
					},
					format: {
						// comments: false,
					},
				},
				extractComments: false,
			})]
		}
	};

	const commonScripts = Object.assign({}, config, {
		name: 'common',
		entry: ['idempotent-babel-polyfill', './src/algolia_search.js'],
		output: {
			path: path.resolve(__dirname, 'dist/'),
			filename: 'algolia_search.bundled.js'
		},
	});

	return commonScripts
}

module.exports = (env, argv) => {
	// isProduction = true
	isProduction = argv.mode == 'production' ? true : false
	return [
		webpackConfig(isProduction)
	];
}