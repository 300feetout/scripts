import {hits, index, configure, refinementList} from 'instantsearch.js/es/widgets'

import {addFilter} from './../html'

import get from 'lodash/get'

export default function({config, trainingHitsContainer, trainingRow, communitySearchInstance}) {
	const trainingIndex = index({
		indexName: config.trainingIndexName || 'thoughtspot_university',
	})

	let target = '_blank'
	if (config.firstResults == 'training' && !config.iframeEmbed) {
		target = '_self'
	}

	const trainingWidgets = [
		configure({
			hitsPerPage: 5
		}),
		hits({
			container: trainingHitsContainer.querySelector('[data-training-container]'),
			templates: {
				item: (hit) => {
					let viewMore = ''
					let popoutButton = ''
					if (!config.hideViewMore) {
						const hitsPerPage = get(communitySearchInstance, 'mainHelper.derivedHelpers[3].lastResults.hitsPerPage')
						if (config.inline && hitsPerPage == 5 && hit.__position == hitsPerPage) {
							viewMore = '<a class="view_more" data-more="training" href="#">View more</a>' 
						}
					}

					if (config.showPopoutButton && hit.__position == 1) {
						let popoutLink = 'https://training.thoughtspot.com/'
						popoutButton = `<a class="popout" aria-label="Open training site" target="_blank" href="${popoutLink}"></a>`
					}

					return `${popoutButton}<a class="result" href="${hit.url}" target="${target}">
						<h3><span class="tag first">${hit.type}</span> ${hit.lesson_title ? hit.lesson_title + ' | ' : ''}${hit.course_title}</h3>
					</a>${viewMore}`
				}
			}
		})
	]

	if (!config.inline) {
		const trainingFilters = document.createElement('div')
		trainingFilters.classList.add('refinement_filters')
		trainingRow.appendChild(trainingFilters)

		const typeFilterContainer = addFilter(trainingFilters, ['training_type'], 'Type')

		trainingWidgets.push(refinementList({
			container: typeFilterContainer,
			attribute: 'type'
		}))
		trainingWidgets.push(refinementList({
			container: addFilter(trainingFilters, ['delivery_type'], 'Delivery'),
			attribute: 'facets.Delivery'
		}))
		trainingWidgets.push(refinementList({
			container: addFilter(trainingFilters, ['deployment_type'], 'Deployment'),
			attribute: 'facets.Deployment'
		}))

		const header = document.createElement('h3')
		header.style.marginBottom = '12px'
		header.innerHTML = 'Training'
		typeFilterContainer.append(header)

	}

	trainingIndex.addWidgets(trainingWidgets)

	return trainingIndex
}
