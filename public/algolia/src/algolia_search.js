import 'idempotent-babel-polyfill'

import '@algolia/autocomplete-theme-classic'

import generateSecureKey from './community/generate_secure_key'

import {generateSearchContainer, decorateSearchWrapper, generateHitsRows} from './html'

import getProfileData from './community/profile'

import {attachCommunityEvents, attachEvents} from './community/events.js'

import forOwn from 'lodash/forOwn'

import createCommunityInstance from './community/instance'
import createDocsInstance from './docs/instance'
import createTrainingInstance from './training/instance'

require('./css/algolia_search.styl');

import viewMoreFilter from './view_more'

const cipher = salt => {
	const textToChars = text => text.split('').map(c => c.charCodeAt(0));
	const byteHex = n => ("0" + Number(n).toString(16)).substr(-2);
	const applySaltToChar = code => textToChars(salt).reduce((a,b) => a ^ b, code);

	return text => text.split('')
		.map(textToChars)
		.map(applySaltToChar)
		.map(byteHex)
		.join('');
}

const decipher = salt => {
	const textToChars = text => text.split('').map(c => c.charCodeAt(0));
	const applySaltToChar = code => textToChars(salt).reduce((a,b) => a ^ b, code);
	return encoded => encoded.match(/.{1,2}/g)
		.map(hex => parseInt(hex, 16))
		.map(applySaltToChar)
		.map(charCode => String.fromCharCode(charCode))
		.join('');
}

(function() {

	const AlgoliaSearchComponent = async function(algoliaSearchWrapper, config) {
		
		// const myCipher = cipher('saltysalt')
		// console.log(myCipher('8e22d8958711f962962f736908615bb3'))
		const myDecipher = decipher('saltysalt')

		// let apiKey = '8e22d8958711f962962f736908615bb3'
		let apiKey = myDecipher('411c4b4b1d41404c414e48481f404f4b404f4b1f4e4a4f4049414f484c1b1b4a')
		let secureApiKey = apiKey
		window.forceSearch = false

		const {caseField, profileName, userId} = getProfileData(config)

		// secureApiKey = await generateSecureKey(userId, caseField, 'Individual Contributor')
		secureApiKey = await generateSecureKey(userId, caseField, profileName)

		decorateSearchWrapper(algoliaSearchWrapper, config)

		const searchContainer = generateSearchContainer(algoliaSearchWrapper)


		const hitsParentContainer = document.createElement('div')
		hitsParentContainer.classList.add('hits_parent_container')
		algoliaSearchWrapper.appendChild(hitsParentContainer)

		const {communityRow, communityHitsContainer, docsRow, docsHitsContainer, trainingRow, trainingHitsContainer} = generateHitsRows({hitsParentContainer, config})

		const {communitySearchInstance, communityWidgets, communityFilters, searchTabsWrapper} = createCommunityInstance({config, secureApiKey, searchContainer, communityHitsContainer, communityRow, hitsParentContainer})


		const trainingIndex = createTrainingInstance({config, trainingRow, trainingHitsContainer, communitySearchInstance})
		const docsIndex = createDocsInstance({config, docsRow, docsHitsContainer, communitySearchInstance})

		communitySearchInstance.trainingIndex = trainingIndex
		communitySearchInstance.docsIndex = docsIndex

		communityWidgets.push(docsIndex)
		communityWidgets.push(trainingIndex)

		communitySearchInstance.addWidgets(communityWidgets)

		communitySearchInstance.start()

		const searchInput = algoliaSearchWrapper.querySelector('input')
		
		searchInput.placeholder = config.placeholderText || ''
		const searchForm = algoliaSearchWrapper.querySelector('form')
		const clearInputButton = document.createElement('button')
		clearInputButton.classList.add('clear_search')
		searchForm.appendChild(clearInputButton)





		const clearSearch = (e, closeResults) => {
			algoliaSearchWrapper.querySelector('.ais-SearchBox-reset').click()
			updateSearchQuery('')
			
			if (closeResults) {
				// clearInputButton.setAttribute('tabindex', '-1')
				searchInput.parentNode.classList.remove('expanded')
				// searchInput.classList.remove('focused')
				this.api.closeSearchResults(config.inline ? true : false)
			} else {
				setTimeout(() => {
					searchInput.focus()
				}, 200)
			}
			if (e) {
				e.preventDefault()
				e.stopPropagation()
			}
		}

		function updateSearchQuery(searchValue) {
			communitySearchInstance.helper.setQuery(searchValue).search()
		}


		attachCommunityEvents({communitySearchInstance, hitsParentContainer, config, communityFilters, searchInput, clearInputButton})
		attachEvents({config, clearSearch, algoliaSearchWrapper, searchTabsWrapper, hitsParentContainer, communityHitsContainer, docsHitsContainer, trainingHitsContainer, communityRow, docsRow, trainingRow, communitySearchInstance, searchInput, clearInputButton})


		this.api = {
			viewMoreFilter: (section) => {
				viewMoreFilter({section, searchTabsWrapper, hitsParentContainer, communityHitsContainer, docsHitsContainer, trainingHitsContainer, communityRow, docsRow, trainingRow, communitySearchInstance})
			},
			focusSearchInput: () => {
				searchInput.focus()
			},
			closeSearchResults: (blur) => {
				hitsParentContainer.style.height = '0px'
				hitsParentContainer.classList.remove('expanded')
				document.body.classList.remove('algolia_hits_expanded')
				updateSearchQuery('')
				setTimeout(() => {
					searchInput.value = ''
					hitsParentContainer.querySelectorAll('a').forEach((a) => {
						a.setAttribute('tabindex', '-1')
					})
				}, 200)

				searchInput.focus()
			},
			selectFilters: (filters) => {
				forOwn(filters, (value, cssClass) => {
					if (!value) return

					const input = algoliaSearchWrapper.querySelector(`.${cssClass} input[value="${value}"]`)
					if (input) {
						input.click()
					}
				})
			},
			setQuery: (searchValue) => {
				// Don't know why, but this is being cleared out immediately after population, so I'm setting a small delay as a workaround.
				setTimeout(() => {
					setTimeout(() => {
						searchInput.value = searchValue
					}, 100)
					updateSearchQuery(searchValue)
				}, 100)
			}
		}

		if (config.startOpen) {
			hitsParentContainer.classList.add('no_transition', 'hide_initially')
		}

		if (config.startFocused) {
			this.api.focusSearchInput()
		}

		return this.api
	}

	window.AlgoliaSearchComponent = AlgoliaSearchComponent

	if (document.createEvent) {
		const event = document.createEvent("HTMLEvents")
		event.initEvent('algoliaReady', true, true)
		event.eventName = 'algoliaReady'
		window.dispatchEvent(event)
	} else {
		const event = document.createEventObject()
		event.eventName = 'algoliaReady'
		event.eventType = 'algoliaReady'
		window.fireEvent("on" + event.eventType, event)
	}
})()
