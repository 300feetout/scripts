function addRow(prepend, hitsParentContainer) {
	const columnRow = document.createElement('div')
	columnRow.classList.add('row')
	if (prepend) {
		hitsParentContainer.prepend(columnRow)
	} else {
		hitsParentContainer.appendChild(columnRow)
	}
	return columnRow
}

function addFilter(parent, extraClass, title) {
	const filterWrapper = document.createElement('div')
	if (title) {
		const header = document.createElement('h4')
		header.innerHTML = title
		filterWrapper.appendChild(header)
	}
	if (extraClass)
		filterWrapper.classList.add(...extraClass)
	parent.appendChild(filterWrapper)

	return filterWrapper
}

module.exports = {
	generateSearchContainer: function(algoliaSearchWrapper) {
		const searchContainer = document.createElement('div')
		searchContainer.classList.add('search_wrapper')
		algoliaSearchWrapper.appendChild(searchContainer)
		return searchContainer
	},

	decorateSearchWrapper: function(algoliaSearchWrapper, config) {
		if (config.addClass) {
			algoliaSearchWrapper.classList.add(...config.addClass.split(' '))
		}

		if (config.inline) {
			algoliaSearchWrapper.classList.add('inline')
		} else {
			algoliaSearchWrapper.classList.add('search_page')
		}
	},

	addRow: addRow,

	addFilter: addFilter,

	generateHitsRows: function({config, hitsParentContainer}) {
		const communityRow = addRow(false, hitsParentContainer)
		const communityHitsContainer = document.createElement('div')
		// const communityFiltersShowMore = document.createElement('button')
		// communityFiltersShowMore.classList.add('show_more')
		communityHitsContainer.id = 'community_hits_container'
		communityHitsContainer.setAttribute('data-hit-container', '')
		communityHitsContainer.innerHTML = '<h2>Community</h2><div data-community-container></div>'
		communityRow.appendChild(communityHitsContainer)

		const docsRow = addRow(false, hitsParentContainer)
		const docsHitsContainer = document.createElement('div')
		docsHitsContainer.id = 'docs_hits_container'
		docsHitsContainer.setAttribute('data-hit-container', '')
		docsHitsContainer.innerHTML = '<h2>Documentation</h2><div data-docs-container></div>'
		docsRow.appendChild(docsHitsContainer)

		const trainingRow = addRow(false, hitsParentContainer)
		const trainingHitsContainer = document.createElement('div')
		trainingHitsContainer.id = 'training_hits_container'
		trainingHitsContainer.setAttribute('data-hit-container', '')
		trainingHitsContainer.innerHTML = '<h2>Training</h2><div data-training-container></div>'
		trainingRow.appendChild(trainingHitsContainer)

		if (config.firstResults == 'training') {
			hitsParentContainer.prepend(trainingRow)
		} else if (config.firstResults == 'docs') {
			hitsParentContainer.prepend(docsRow)
		}

		const moreButton = document.createElement('div')
		moreButton.innerHTML = 'More'
		moreButton.setAttribute('tabindex', '-1')
		moreButton.classList.add('more_hits_button')
		hitsParentContainer.appendChild(moreButton)

		return {communityRow, communityHitsContainer, docsRow, docsHitsContainer, trainingRow, trainingHitsContainer}
	}
}