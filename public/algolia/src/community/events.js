import viewMoreFilter from '../view_more'

function setExplicitHeights({communityFilters, height}) {
	communityFilters.querySelectorAll('.additional_filter').forEach((filter) => {	
		if (height) {
			filter.style.height = height
		} else {
			const calculatedHeight = filter.childNodes[0].offsetHeight + (filter.childNodes[1] ? filter.childNodes[1].offsetHeight + 15 : 0) + 'px'
			filter.style.height = calculatedHeight
		}
	})
}

function getResultsHeight({hitsParentContainer}) {
	let totalHeight = 0
	hitsParentContainer.querySelectorAll('.row').forEach((row) => {
		totalHeight += row.offsetHeight
	})
	return totalHeight ? totalHeight + 10 + 'px' : '0px'
}


export function attachEvents({config, algoliaSearchWrapper, searchTabsWrapper, hitsParentContainer, communityHitsContainer, docsHitsContainer, trainingHitsContainer, communityRow, docsRow, trainingRow, communitySearchInstance, searchInput, clearInputButton, clearSearch}) {
		clearInputButton.addEventListener('mousedown', (e) => {
			clearSearch(e, config.inline ? true : false)
			e.preventDefault()
			e.stopPropagation()
		})

		clearInputButton.addEventListener('click', (e) => {
			clearSearch(e, config.inline ? true : false)
			e.preventDefault()
			e.stopPropagation()
		})

		clearInputButton.addEventListener('touchstart', (e) => {
			clearSearch(e, config.inline ? true : false)
			e.preventDefault()
			e.stopPropagation()
		})


		if (!config.inline) {
			searchTabsWrapper.addEventListener('click', (e) => {
				const section = e.target.dataset.section
				viewMoreFilter({section, searchTabsWrapper, hitsParentContainer, communityHitsContainer, docsHitsContainer, trainingHitsContainer, communityRow, docsRow, trainingRow, communitySearchInstance})
				return false
			})
		}

		if (config.inline) {
			function viewMoreClicked(e) {
				const target = e.target
				if (target.dataset.more) {
					if (config.viewMoreFunction) {
						config.viewMoreFunction(target.dataset.more, searchInput.value, this)
					} else {
						viewMoreFilter({section: target.dataset.more, searchTabsWrapper, hitsParentContainer, communityHitsContainer, docsHitsContainer, trainingHitsContainer, communityRow, docsRow, trainingRow, communitySearchInstance})
						target.remove()
					}
					e.preventDefault()
					e.stopPropagation()
				}
			}
			const moreButton = hitsParentContainer.querySelector('.more_hits_button')
			if (moreButton) {
				moreButton.addEventListener('click', (e) => {
					e.preventDefault()
					hitsParentContainer.scroll({top: hitsParentContainer.scrollHeight, behavior: "smooth"})
				})
			}
			hitsParentContainer.addEventListener('click', viewMoreClicked)
		}
		
		if (!config.inline && config.onCloseButton) {
			const closeButton = document.createElement('button')
			closeButton.classList.add('close')
			algoliaSearchWrapper.appendChild(closeButton)

			closeButton.addEventListener('click', () => {
				config.onCloseButton()
			})

			document.body.addEventListener('keydown', (e) => {
				if (e.key == 'Escape') {
					if (!searchInput.value || e.target != searchInput) {
						config.onCloseButton()
					}
				}
			})
		}

	}

export function attachCommunityEvents({communitySearchInstance, hitsParentContainer, config, communityFilters, clearInputButton, searchInput}) {
	communitySearchInstance.on('render', () => {

		function onScroll(e) {
			if (hitsParentContainer.scrollHeight - 40 < hitsParentContainer.clientHeight + hitsParentContainer.scrollTop) {
				hitsParentContainer.classList.remove('more')
			} else {
				hitsParentContainer.classList.add('more')
			}
		}

		if (hitsParentContainer.classList.contains('expanded')) {
			setTimeout(() => {
				hitsParentContainer.style.height = getResultsHeight({hitsParentContainer})

				if (parseInt(hitsParentContainer.style.height) > parseInt(getComputedStyle(hitsParentContainer).maxHeight)) {
					hitsParentContainer.classList.add('more')
					hitsParentContainer.addEventListener('scroll', onScroll)
				} else {
					hitsParentContainer.classList.remove('more')
					hitsParentContainer.removeEventListener('scroll', onScroll)
				}				
			}, 100)
		}

		if (config.inline == false && communityFilters && communityFilters.querySelector('.more_filters.expand')) {
			setExplicitHeights({communityFilters})
		}
	})

	function onSearchInput(e) {

		// We can probably squeeze out some more efficiency by disabling force-search after the initial focus
		if (e.type == 'focus') {
			window.forceSearch = true
			communitySearchInstance.helper.search()
		}

		if (!window.focusedBefore || searchInput.value) {
			// clearInputButton.setAttribute('tabindex', '')

			searchInput.parentNode.classList.add('expanded')
			searchInput.classList.add('focused')
			
			hitsParentContainer.classList.add('expanded')
			document.body.classList.add('algolia_hits_expanded')
			hitsParentContainer.style.height = getResultsHeight({hitsParentContainer})
			hitsParentContainer.querySelectorAll('a').forEach((a) => {
				a.setAttribute('tabindex', '')
			})
			
		}

		setTimeout(() => {
			hitsParentContainer.classList.remove('no_transition')
			hitsParentContainer.classList.remove('hide_initially')
		}, 300)

		window.focusedBefore = true
	}

	searchInput.addEventListener('input', onSearchInput)
	searchInput.addEventListener('focus', onSearchInput)
}