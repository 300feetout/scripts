export default async function generateSecureKey(userId, caseField, profileName) {
	let keyQueryString = ''
	if (userId && caseField) {
		keyQueryString = `?userId=${userId}&caseField=${caseField}&userField=owner_id`
	} else if (userId) {
		keyQueryString = `?userId=${userId}&userField=owner_id`
	}

	if (profileName) {
		keyQueryString += (keyQueryString[0] == '?' ? '&' : '?') + `role=${profileName}&roleField=role_only_roles`
	}

	// console.log('keyQueryString', keyQueryString)

	// const response = await fetch(`https://thoughtspotnext.ngrok.io/api/algolia_secure_key${keyQueryString}`)
	const response = await fetch(`https://next.thoughtspot.us/api/algolia_secure_key${keyQueryString}`)
	const secureApiKey = await response.text()

	return secureApiKey
}