import instantsearch from 'instantsearch.js'
import algoliasearch from 'algoliasearch/lite'

import {searchBox, hits, index, configure, refinementList, toggleRefinement} from 'instantsearch.js/es/widgets'

import {communityHitTemplates} from './rendering'

import flatpickr from "flatpickr"
import 'flatpickr/dist/themes/light.css'

import {addRow, addFilter} from './../html'

import {connectRange} from 'instantsearch.js/es/connectors'

import {fromUnixTime, format} from 'date-fns'

export default function({config, secureApiKey, searchContainer, communityRow, communityHitsContainer, hitsParentContainer}) {
	
	let communityFilters = null
	let communityMoreFilters = null
	let searchTabsWrapper = null

	const communitySearchInstance = instantsearch({
		indexName: config.communityIndexName || 'Community',
		searchClient: algoliasearch('FFSASUS8DI', secureApiKey),
		searchFunction(helper) {
			if (helper.state.query || forceSearch) {
				helper.search()
			}
		}
	});

	let communityWidgets = [
		configure({
			hitsPerPage: 5
		}),
		hits({
			container: communityHitsContainer.querySelector('[data-community-container]'),
			templates: communityHitTemplates({config, communitySearchInstance}),
		}),
	]

	if (!config.inline) {
		communityFilters = document.createElement('div')
		communityMoreFilters = document.createElement('div')
		communityMoreFilters.classList.add('more_filters')
		communityMoreFilters.classList.add('expand') // Start expanded
		communityFilters.classList.add('refinement_filters')
		// communityFiltersShowMore.innerHTML = 'Show filters'
		communityRow.appendChild(communityFilters)

		const tabsRow = addRow(true, hitsParentContainer)
		searchTabsWrapper = document.createElement('div')
		searchTabsWrapper.classList.add('filter_tabs')
		tabsRow.appendChild(searchTabsWrapper)
		searchTabsWrapper.innerHTML = `<a href="#all" data-section="all">All</a>
			<a href="#docs" data-section="docs">Documentation</a>
			<a href="#community" data-section="community">Community</a>
			<a href="#training" data-section="training">Training</a>`

		const topicsContainer = addFilter(communityFilters, ['topics_filter'], 'Topics')

		communityWidgets.push(refinementList({
			container: topicsContainer,
			attribute: 'topics_for_refinement',
			limit: 5,
			showMoreLimit: 10,
			showMore: true,
			templates: {
				showMoreText: (props) => {
						return props.isShowingMore ? 'Show less' : 'Show more'
				},
			}
		}))

		const objectTypeFilter = addFilter(communityFilters, ['additional_filter', 'object_type_filter', 'expand'], 'Content type')


		communityWidgets.push(refinementList({
			container: objectTypeFilter,
			attribute: 'object_type',
			limit: 10,
			showMore: false,
			transformItems(items) {
				return items.map(item => {
					let label = item.label
					switch (item.label) {
						case 'FeedItemQuestionPost':
							label = 'Discussions'
							break;
						case 'FeedItemTextPost':
							label = 'Comment'
							break;
						case 'CaseComment':
							label = 'Case Comment'
							break;
						case 'KnowledgeArticle':
							label = 'Article'
							break;
					}
					return {
						...item,
						highlighted: label,
					}
				})
			},
		}))

		const bestCommentFilter = addFilter(communityFilters, ['additional_filter', 'best_comment_filter', 'expand'], 'Show content')
		communityWidgets.push(toggleRefinement({
			container: bestCommentFilter,
			attribute: 'has_best_comment',
			on: true,
			templates: {
				labelText: 'With Best Answer'
			}
		}))

		const hasCommentsFilter = addFilter(communityFilters, ['additional_filter', 'has_comments_filter', 'expand'])
		communityWidgets.push(toggleRefinement({
			container: hasCommentsFilter,
			attribute: 'has_comments',
			templates: {
				labelText: 'With Comments'
			}
		}))

		const hasNoCommentsFilter = addFilter(communityFilters, ['additional_filter', 'has_no_comments_filter', 'expand'])
		communityWidgets.push(toggleRefinement({
			container: hasNoCommentsFilter,
			attribute: 'has_no_comments',
			templates: {
				labelText: 'Without Comments'
			}
		}))

		let formattedStartDate = ''
		let formattedEndDate = ''

		const renderDateRangeInput = (renderOptions, isFirstRender) => {
			const {start, range, refine, widgetParams} = renderOptions
			const [min, max] = start
			
			if (isFirstRender) {
				const form = document.createElement('form')
				form.classList.add('community_datepicker')

				widgetParams.container.appendChild(form)
				refine([min, max]) // Set initial value to include everything.
				return
			}
			
			const form = widgetParams.container.querySelector('form')
			form.innerHTML = `
				<input class="datepicker_input" type="text" />
				<input data-min
					tabindex="0"
					type="text"
					name="min"
					value="${formattedStartDate}"
					placeholder="Min date"
				/>
				<span>to</span>
				<input data-max
					tabindex="0"
					type="text"
					name="max"
					value="${formattedEndDate}"
					placeholder="Max date"
				/>
				<input type="submit" hidden />
			`

			const startDate = form.querySelector('[data-min]')
			const endDate = form.querySelector('[data-max]')

			form.flatpickr = flatpickr(form.querySelector('.datepicker_input'), {
				mode: "multiple",
				dateFormat: "U",
				onChange: (selectedDates, dateStr, instance) => {
					let dateTimestamps = dateStr.split(',')
					dateTimestamps = dateTimestamps.map((timestamp) => {return timestamp.trim()})
					
					if (dateTimestamps[0]) {
						formattedStartDate = format(fromUnixTime(dateTimestamps[0]), 'LLL do, yyyy')
						startDate.value = formattedStartDate
					}
					if (dateTimestamps[1]) {
						formattedEndDate = format(fromUnixTime(dateTimestamps[1]), 'LLL do, yyyy')
						endDate.value = formattedEndDate
					}

					if (dateTimestamps.length == 2) {
						instance.close()
						instance.clear()
						refine(dateTimestamps.map(timestamp => timestamp * 1000))
					}
				}
			})
		}
			
		// Create the custom widget
		const customRangeInput = connectRange(renderDateRangeInput)

		const dateFilter = addFilter(communityFilters, ['additional_filter', 'date_filter', 'expand'], 'Date')
		communityWidgets.push(customRangeInput({
			container: dateFilter,
			attribute: 'merged_date',
			min: 0,
			max: 999999999999999
		}))


		const additionalFilterGroup = document.createElement('div')
		additionalFilterGroup.classList.add('additional_filter_group')

		communityMoreFilters.appendChild(objectTypeFilter)

		additionalFilterGroup.appendChild(bestCommentFilter)
		additionalFilterGroup.appendChild(hasCommentsFilter)
		additionalFilterGroup.appendChild(hasNoCommentsFilter)

		communityMoreFilters.appendChild(additionalFilterGroup)
		communityMoreFilters.appendChild(dateFilter)

		const header = document.createElement('h3')
		header.innerHTML = 'Community'
		topicsContainer.append(header)

		communityFilters.appendChild(communityMoreFilters)
	}

	// We add an Index for the community index despite it being the main index for our instantsearch object so we can control its settings independent of the other indexes.
	const communityIndex = index({
		indexName: 'Community',
	}).addWidgets(communityWidgets)

	communityWidgets = 	[
		searchBox({
			container: searchContainer,
		}),
		communityIndex
	]

	communitySearchInstance.communityIndex = communityIndex

	return {communitySearchInstance, communityWidgets, communityFilters, communityMoreFilters, searchTabsWrapper}
}