import striptags from 'striptags'

import get from 'lodash/get'

import truncate from 'truncate'

const baseUrl = 'https://community.thoughtspot.com/customers/s/'

function decodeHtmlEntities(html) {
	// Create a temporary textarea element
	const textarea = document.createElement('textarea');
	// Set the provided HTML encoded string as its content
	textarea.innerHTML = html;
	// The browser automatically decodes the content of the textarea, so return it
	return textarea.value;
}

export function communityHitTemplates({config, communitySearchInstance}) {

	let target = '_blank'
	if (config.firstResults == 'community' && !config.iframeEmbed) {
		target = '_self'
	}

	return {
		item: (hit) => {
			let template = ''
			let popoutButton = ''

			if (hit.object_type == 'FeedItemTextPost') {

				let matchedText = []
				if (hit.parent_type == 'CollaborationGroup') {
					const content = decodeHtmlEntities(hit._highlightResult.body.value)
					const regex = /(<mark>.*?<\/mark>)/gm
					const surroundingChars = 30
					// Split matchedString into an array of strings, each string is either a match or not a match
					const matches = content.match(regex)
					if (matches) {
						matches.forEach((matchedStringPart, index) => {
							const matchIndex = content.indexOf(matchedStringPart)
							const matchStartIndex = matchIndex
							const matchEndIndex = matchIndex + matchedStringPart.length
							const preMatchLength = matchIndex - surroundingChars >= 0 ? surroundingChars : matchIndex
							const postMatchLength = (matchIndex + matchedStringPart.length + surroundingChars) <= content.length ? surroundingChars : content.length - matchEndIndex

							let preMatchString = striptags(content.slice(0, matchStartIndex))
							let postMatchString = striptags(content.slice(matchEndIndex))
							preMatchString = preMatchString.slice(-preMatchLength)
							postMatchString = postMatchString.slice(0, postMatchLength)

							matchedText.push(preMatchString + matchedStringPart + postMatchString)
						})
					}
				}

				const communityUrl = 'https://community.thoughtspot.com/customers/s/feed/'
					template = `<a class="result" href="${communityUrl}${hit.id}" target="${target}">
									<h3><span class="tag first">Post</span>${hit.title || hit.parent_name}</h3>
									${matchedText.length ? `<p class="matches">... ${matchedText.slice(0,5).join(' ... ')} ...</p>` : ''}
								</a>`
			} else if (hit.object_type == 'FeedItemQuestionPost') {
				const communityUrl = 'https://community.thoughtspot.com/customers/s/question/'
				template = `<a class="result" href="${communityUrl}${hit.id}" target="${target}">
								<h3><span class="tag first">Discussions</span>${hit.title || hit.body}</h3>
							</a>`
			} else if (hit.object_type == 'FeedItemLead') {
				const communityUrl = `${baseUrl}lead/`
				template = `<a class="result" href="${communityUrl}${hit.parent_id}" target="${target}">
								<h3><span class="tag first">lead: ${hit.parent_name}</span> ${hit.body}</h3>
							</a>`
			} else if (hit.object_type == 'Lead') {
				const communityUrl = `${baseUrl}lead/`
				template = `<a class="result lead" href="${communityUrl}${hit.id}" target="${target}">
								<img src="https://community.thoughtspot.com/customers${hit.photo_url}" />
								<h3>${hit.name}</h3>
								<p class="last_activity_date"><span>last activity:</span> ${hit.last_activity_date}</p>
								<p class="email"><span>email:</span> ${hit.email}</p>
								<p><span>phone:</span> ${hit.phone}</span></p>
								<p><span>industry:</span> ${hit.industry}</span></p>
								<p><span>location:</span> ${hit.country}${hit.state ? ' , ' + hit.state : ''}</span></p>
							</a>`
			} else if (hit.object_type == 'KnowledgeArticle') {
				const communityUrl = `${baseUrl}article/`
				template = `<a class="result" href="${communityUrl}${hit.url_name}" target="${target}">
								<h3><span class="tag first">Article</span> ${hit.title}</h3>
							</a>`
			} else if (hit.object_type == 'Group') {
				const communityUrl = `${baseUrl}group/`
				template = `<a class="result" href="${communityUrl}${hit.id}" target="${target}">
								<h3><span class="tag first">Group</span>${hit.name}</h3>
							</a>`
			} else if (hit.object_type == 'Case') {
				const communityUrl = `${baseUrl}case/`
				template = `<a class="result" href="${communityUrl}${hit.id}" target="${target}">
								<h3><span class="tag first">Case</span> ${hit.subject}</h3>
							</a>`
			} else if (hit.object_type == 'CaseComment') {
				const communityUrl = `${baseUrl}case/`
				template = `<a class="result" href="${communityUrl}${hit.parent_id}" target="${target}">
								<h3><span class="tag first">Case Comment</span> ${hit.parent_subject}</h3>
							</a>`
			} else if (hit.object_type == 'File') {
				const communityUrl = `${baseUrl}contentdocument/`
				let preview = ''
				if (['PNG', 'SVG', 'JPG', 'JPEG', 'GIF'].indexOf(hit.file_type) > -1) {
					preview = `<img src="https://community.thoughtspot.com/customers/sfc/servlet.shepherd/version/renditionDownload?rendition=ORIGINAL_Png&versionId=${hit.latest_published_version_id}&operationContext=CHATTER" />`
				}
				template = `<a class="result file" href="${communityUrl}${hit.id}" target="${target}">
								<h3><span class="tag first">${hit.file_type}</span> ${hit.title}</h3>
								${preview}
							</a>`
			} else if (hit.object_type == 'IdeaComment') {
				const communityUrl = `https://community.thoughtspot.com/s/idea/${hit.idea_id}/detail`
				template = `<a class="result" href="${communityUrl}" target="${target}">
								<h3><span class="tag first">Idea Comment</span> ${truncate(striptags(hit.comment_body), 240)}</h3>
							</a>`
			} else if (hit.object_type == 'Idea') {
				const communityUrl = `https://community.thoughtspot.com/s/idea/${hit.id}/detail`
				template = `<a class="result" href="${communityUrl}" target="${target}">
								<h3><span class="tag first">Idea</span> ${hit.title}</h3>
							</a>`
			}

			const hitsPerPage = get(communitySearchInstance, 'mainHelper.derivedHelpers[1].lastResults.hitsPerPage')
			if (!config.hideViewMore) {
				template = config.inline && hitsPerPage == 5 && hit.__position == hitsPerPage ? template + '<a class="view_more" data-more="community" href="#">View more</a>' : template
			}

			if (config.showPopoutButton && hit.__position == 1) {
				let popoutLink = 'https://community.thoughtspot.com/'
				popoutButton = `<a class="popout" aria-label="Open community site" target="_blank" href="${popoutLink}"></a>`
			}

			return popoutButton + template
		}
	}
}