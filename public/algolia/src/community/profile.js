import get from 'lodash/get'

export default function getProfileData(config) {
	let caseField = null
	let userId = get(config, 'user.id')

	const profileName = get(config, 'user.profileName')
	if (profileName == 'ThoughtSpot Customer Community User: Manager') {
		caseField = 'account_id'
		userId = get(config, 'user.accountId')
	} else if (profileName == 'ThoughtSpot Customer Community User: Individual Contributor') {
		caseField = 'contact_id'
		userId = get(config, 'user.contactId')
	}

	return {caseField, profileName, userId}
}

