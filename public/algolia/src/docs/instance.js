import {hits, index, configure, refinementList} from 'instantsearch.js/es/widgets'

import {addFilter} from './../html'

import get from 'lodash/get'
import forOwn from 'lodash/forOwn'

export default function({config, docsHitsContainer, docsRow, communitySearchInstance}) {

	let docsFiltersConfig = []
	if (get(config, 'filters.docs')) {
		forOwn(get(config, 'filters.docs'), (value, key) => {
			docsFiltersConfig.push(`${key}:${value}`)
		})
	}

	const docsIndex = index({
		indexName: config.docsIndexName || 'crawler_thoughtspot_documentation'
	})

	let target = '_blank'
	if (config.firstResults == 'docs' && !config.iframeEmbed) {
		target = '_self'
	}

	const docsWidgets = [
		configure({
			hitsPerPage: 5,
			filters: docsFiltersConfig.join(' AND ')
		}),
		hits({
			container: docsHitsContainer.querySelector('[data-docs-container]'),
			templates: {
				item: (hit) => {
					let viewMore = ''
					let popoutButton = ''
					const hitsPerPage = get(communitySearchInstance, 'mainHelper.derivedHelpers[2].lastResults.hitsPerPage')
					if (!config.hideViewMore) {
						if (config.inline && hitsPerPage == 5 && hit.__position == hitsPerPage) {
							viewMore = '<a class="view_more" data-more="docs" href="#">View more</a>' 
						}
					}

					if (config.showPopoutButton) {
						let popoutLink = 'https://docs.thoughtspot.com/'
						try {
							if (hit.__position == 1) {
								// const iframeLink = document.querySelector('[rel="canonical"]').href
								// const version = iframeLink.match(/.*?netlify\.app\/cloud\/([\w.\d]+)\//)[1]
								// popoutLink = `https://docs.thoughtspot.com/cloud/${version}/notes`
								popoutButton = `<a class="popout" aria-label="Open docs site" target="_blank" href="${popoutLink}"></a>`
							}
						} catch (e) {
							popoutButton = `<a class="popout" aria-label="Open docs site" target="_blank" href="${popoutLink}"></a>`
						}
					}

					return `${popoutButton}<a class="result" href="${hit.url}" target="${target}">
						<h3><span class="tag first">${hit.version}</span> ${hit.title}</h3>
					</a>${viewMore}`
				},
			},
		})
	]

	if (!config.inline) {
		const docsFilters = document.createElement('div')
		docsFilters.classList.add('refinement_filters')
		docsRow.appendChild(docsFilters)

		const versionFilterContainer = addFilter(docsFilters, ['docs_version'], 'Version')
		docsWidgets.push(refinementList({
			container: versionFilterContainer,
			attribute: 'version',
		}))
		docsWidgets.push(refinementList({
			container: addFilter(docsFilters, ['deployment'], 'Deployment'),
			attribute: 'deployment'
		}))

		const header = document.createElement('h3')
		header.style.marginBottom = '12px'
		header.innerHTML = 'Documentation'
		versionFilterContainer.append(header)
	}

	docsIndex.addWidgets(docsWidgets)

	return docsIndex
}
