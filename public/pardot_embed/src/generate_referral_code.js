function querystringContains(search) {
	return window.location.search.indexOf(search) > -1
}

function compareUrl(url1, url2) {
	return url1 == url2 || url1 == url2.slice(0,-1) ? true : false
}

const referrerMap = [
	function() {
		if (compareUrl(document.referrer, 'https://www.bing.com/') && querystringContains('msclkid')) {
			return 'Paid'
		} else if (compareUrl(document.referrer, 'https://www.google.com/') && querystringContains('gclid')) {
			return 'Paid'
		} else if (querystringContains('utm_medium=onlineads') || querystringContains('utm_medium=paidsocial')) {
			return 'Paid'
		}

		return false
	},
	function() {
		if (compareUrl(document.referrer, 'https://www.bing.com/')) {
			return 'Search'
		} else if (compareUrl(document.referrer, 'https://www.google.com/')) {
			return 'Search'
		} else if (compareUrl(document.referrer, 'https://duckduckgo.com/')) {
			return 'Search'
		}
		return false
	},
	function() {
		return compareUrl(document.referrer, 'https://www.zoom.com/') ? 'Zoom' : false
	},
	function() {
		if (compareUrl(document.referrer, 'https://www.linkedin.com/')) {
			return 'Social'
		} else if (compareUrl(document.referrer, 'https://www.t.co/')) {
			return 'Social'
		} else if (compareUrl(document.referrer, 'https://www.youtube.com/')) {
			return 'Social'
		} else if (compareUrl(document.referrer, 'https://www.facebook.com/')) {
			return 'Social'
		}
		return false
	}
]

module.exports = function(urlParams) {
	const tsrefValue = urlParams.get('tsref')

	const trackingStringArray = []

	trackingStringArray.push(`loc_page`)
	
	trackingStringArray.push(`page_${window.location.pathname.slice(1).slice(0, 15)}`)

	try {
		if (document.referrer) {
			let source = null
			for (let i = 0; i < referrerMap.length; i++) {
				source = referrerMap[i]()
				if (source) {
					trackingStringArray.push(`source_${source}`)
					break;
				}
			}
			if (!source) {
				trackingStringArray.push('source_Other_' + encodeURIComponent(document.referrer.slice(0,30)))
			}
		} else {
			trackingStringArray.push('source_Direct')
		}
	} catch (e) {console.log('pardot_embed exception', e)}


	trackingStringArray.push(`tsref_${tsrefValue || null}`)

	trackingStringArray.push(`tsiref_${urlParams.get('tsiref') || null}`)

	return trackingStringArray.join('+')
}