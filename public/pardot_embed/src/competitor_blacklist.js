module.exports = [
	"mu-sigma.com", "hyperanna.com", "unscrambl.com", "21ct.com","actian.com","alteryx.com","answerrocket.com","avanade.com","beyondcore.com","birst.com","sap.com","contify.com","progress.com","altair.com","domo.com","finchcomputing.com","forcebi.com","aeratechnology.com","gooddata.com","logility.com","hindsait.com","incorta.com","ibi.com","scuba.io","izenda.com","jaspersoft.com","looker.com","microsoft.com","microstrategy.com","mondobrain.com","navport.com","opentext.com","oracle.com","orcatec.com","osrikgroup.com","paxata.com","hitachivantara.com","planetos.com","platfora.com","pyramidanalytics.com","qlik.com","quad.com","wiser.com","redhat.com","magento.com","saama.com","salesforce.com","samuraidata.com","sas.com","sigmacomputing.com","sisense.com","slalom.com","apache.org","splunk.com","tableau.com","tableausoftware.com","talend.com","tellius.com","tibco.com","ayfie.com","f6s.com","wizni.com","yellowfinbi.com","zoomdata.com",
	"vusra.com",
	"zohomail.eu",
	"mailree.live", "gurumail.xyz", "coffeetimer24.com", "mxgsby.com", "neragez.com", "nubenews.com", "xeiex.com",
	"looqlake.com", "zwoho.com", "arcticdata.cn"
]