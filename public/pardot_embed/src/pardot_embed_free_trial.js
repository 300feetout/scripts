import 'url-search-params-polyfill'
import validate from 'validate.js'
import dayjs from 'dayjs'

const utc = require('dayjs/plugin/utc')
const timezone = require('dayjs/plugin/timezone')
dayjs.extend(utc)
dayjs.extend(timezone)

// const domainWhitelist = require('./domain_whitelist.js')

const countryBlacklist = require('./country_blacklist.js')
const blackList = require('./email_blacklist.js')
const competitors = require('./competitor_blacklist.js')
const roleEmailList = require('./email_role_blacklist.js')
const personalEmailsList = require('./personal_emails.js')

var urlParams = new URLSearchParams(window.location.search);

if (urlParams.get('businessEmailsOnly') !== null) {
	if (isNaN(urlParams.get('businessEmailsOnly'))) { 
		window.businessEmailsOnly = urlParams.get('businessEmailsOnly') == 'true' ? 1 : 0
	} else {
		window.businessEmailsOnly = parseInt(urlParams.get('businessEmailsOnly'))
	}
}

if (urlParams.get('googleSignin') !== null) {
	if (isNaN(urlParams.get('googleSignin'))) { 
		window.googleSignin = urlParams.get('googleSignin') == 'true' ? 1 : 0
	} else {
		window.googleSignin = parseInt(urlParams.get('googleSignin'))
	}
}

const generateReferralCode = require('./generate_referral_code.js')

console.log('INITIALIZED')

if (!window.pardotEmbedInitialized) {
	const search = window.location.search
	// const search = '?use_staging_script=staging'
	if (!window.loadedLocalScript && search.indexOf('use_local_script=1') > -1) {
		console.log('loading_local_script')
		window.loadedLocalScript = true
		const localScript = document.createElement('script')
		localScript.src = 'https://thoughtspotscripts.ngrok.io/public/pardot_embed/dist/pardot_embed_free_trial.bundled.js'
		document.body.appendChild(localScript)
	} else if (!window.loadedStagingScript && search.indexOf('use_staging_script=') > -1) {
		const scriptSuffix = search.match(/use_staging_script=([^&]+)/)[1]
		console.log('loading_staging_script', scriptSuffix)
		window.loadedStagingScript = true
		const stagingScript = document.createElement('script')
		stagingScript.src = `https://scripts.thoughtspot.com/pardot_embed_free_trial_${scriptSuffix}.bundled.js`
		document.body.appendChild(stagingScript)
	} else {

		window.pardotEmbedInitialized = true
		require('./pardot_embed.styl');

		const WebFont = require('../node_modules/webfontloader/webfontloader.js');

		WebFont.load({
			custom: {
				families: ['Optimo-Plain', 'BB Roller Mono'],
				urls: ['https://thoughtspot.com/sites/all/themes/thoughtspot_new/fonts/fonts.css']
			},
		});

		(function() {
			let GDPRMessage = 'I consent to ThoughtSpot, or its partners, contacting me via phone or email for marketing purposes. These include events, white papers, and other content. I understand that I can opt out at any time.';

			const $ = $ || pardot.$

			pardot.$(document).ready(function() {
				// Used for GDPR
				const gdprCountries = ["Norway","Japan","Austria","Italy","Belgium","Latvia","Bulgaria","Lithuania","Croatia","Luxembourg","Cyprus","Malta","Czech","Republic","Netherlands","Denmark","Poland","Estonia","Portugal","Finland","Romania","France","Slovakia","Germany","Slovenia","Greece","Spain","Hungary","Sweden","Ireland","United Kingdom", "Australia", "New Zealand", "Singapore"]

				const countryShortName = ["BE", "BG", "CZ", "DK", "DE", "EE", "IE", "EL", "ES", "FR", "HR", "IT", "CY","LV", "LT", "LU", "HU", "MT", "NL", "AT", "PL", "PT", "RO", "SI", "SK", "FI","SE", "GB"]
				const canadianProvinceNames = ['Alberta', 'British Columbia', 'Manitoba', 'New Brunswick', 'Newfoundland', 'Nova Scotia', 'Nunavut', 'Ontario', 'Prince Edward Island', 'Quebec', 'Saskatchewan', 'Yukon Territory', 'Northwest Territories']
				let canadianProvinces = $('')
				let usaStates = $('')

				let afterParseValue = ''
				let ipCountry = ''
				let ipCountryCode = ''
				let googleSignup = false
				let partnerParams = null


				let hasGoogleSession = false

				const form = $('form#pardot-form')

				const submitP = form.find('p.submit')
				const selectFields = form.find('.pd-select select')
				const inputFields = form.find('.pd-text input')

				const submitButton = form.find('input[type="submit"]')
				const companyField = form.find('#Companypi_Company')

				const langMatch = window.location.search.match(/lang=([^&]+)/)
				let pageLang = $('html').attr('lang') || window.formLang || (langMatch ? langMatch[1] : '') || 'en'

				const var2input = form.find('.Variable_2 input')
				if (var2input) {
					var2input.val(dayjs.utc().tz('America/Los_Angeles').format('YYYY-MM-DD'))
				}

				let additionalMixpanelParams = window.location.search.match(/mixpanelProps=([^&]+)/)
				let additionalMixpanelParamsObject = {}
				if (additionalMixpanelParams && additionalMixpanelParams[1]) {
					try {
						additionalMixpanelParams = decodeURIComponent(additionalMixpanelParams[1])
						additionalMixpanelParams = additionalMixpanelParams.split(',').map((paramGroup) => {
							const paramGroupSplit = paramGroup.split(':')
							additionalMixpanelParamsObject[paramGroupSplit[0]] = paramGroupSplit[1]
						})
						
					} catch(e) {
						console.log('Failed to parse params', e)
					}
				}

				const alreadyHaveAccount =  $('.already_have_account')
				if (alreadyHaveAccount.length) {
					alreadyHaveAccount.on('click', (e) => {
						let signInContext = 'page'
						if ((urlParams ? urlParams.get('mixpanelProps') || '' : '').indexOf('modal') > -1) {
							signInContext = 'modal'
						}
						sendMixpanelEvent('sign-in-button-clicked', {signin_context: signInContext})
					})
				}


				// Run on pardot landing pages, but not iframe embeds. Duplicate the referral code from the pardot_embed component
				if (!window.parent || window.parent == window) {
					const referralCode = generateReferralCode(urlParams)
					document.querySelector('.TS_GA_UTM_Referral_Code input').value = referralCode

					additionalMixpanelParamsObject.trial_registration_context = 'page'
				}

				const sendMixpanelEvent = function(message, data) {
		
					Object.assign(data, additionalMixpanelParamsObject)

					if (typeof mixpanel == 'object' && mixpanel.track) {
						mixpanel.track(message, data)
					} else if (window.parent) {
						if (message)
							data.mixpanelEvent = message

						// console.log('Send mixpanel event', data)
						window.parent.postMessage({
							event_id: 'mixpanel',
							data: data
						}, '*')
					}

				}

				form.find(".TS_URL input").val(window.location.origin + window.location.pathname)

				const mixpanelEvent = function(e, message, data) {
					if (e) {
						const formElem = $(e.target)

						// console.log('mixpanelEvent', e)

						sendMixpanelEvent(message, {
							type: e.type,
							id: formElem.attr('id'),
							name: formElem.is('select') ? formElem.find('option').eq(0).text() : formElem.attr('placeholder'),
							value: formElem.is('select') ? formElem.find('option:selected').text() : formElem.val(),
						})
					} else {
						sendMixpanelEvent(message, data)
					}
				}

				const formSubmittedMixpanelMessage = function(message) {
					mixpanelEvent(null, message, {
						email: emailField.val(),
						company: companyField.val(),
						jobTitle: jobRoleField.find('option:selected').text(),
						cdw: saasDataWarehouseField ? saasDataWarehouseField.find('option:selected').text() : '',
						tsref: document.querySelector('.TS_GA_UTM_Referral_Code input').value,
						trial_registration_method: googleSignup ? 'google_signup' : 'business_email'
					})
				}

				const formErrorMixpanelMessage = function(message, error) {
					mixpanelEvent(null, message, {
						userVisibleError: error,
						email: emailField.val(),
						company: companyField.val(),
						jobTitle: jobRoleField.find('option:selected').text(),
						cdw: saasDataWarehouseField ? saasDataWarehouseField.find('option:selected').text() : '',
						tsref: document.querySelector('.TS_GA_UTM_Referral_Code input').value,
						trial_registration_method: googleSignup ? 'google_signup' : 'business_email'

					})
				}

				let delayListeners = true
				setTimeout(() => {
					delayListeners = false
				}, 1000)

				// Track interactions for mixpanel
				if (inputFields) {
					inputFields.each(function() {
						$(this).on('focus', (e) => {
							if (!delayListeners) {
								mixpanelEvent(e, 'Trial Pardot Form Interaction')
							}
						})
					})
					inputFields.each(function() {
						$(this).on('change', (e) => {
							if (!delayListeners) {
								mixpanelEvent(e, 'Trial Pardot Form Interaction')
							}
						})
					})
				}
				if (selectFields) {
					selectFields.each(function() {
						$(this).on('focus', (e) => {
							if (!delayListeners) {
								mixpanelEvent(e, 'Trial Pardot Form Interaction')
							}
						})
					})
					selectFields.each(function() {
						$(this).on('change', (e) => {
							if (!delayListeners) {
								mixpanelEvent(e, 'Trial Pardot Form Interaction')
							}
						})
					})
				}


				// const tsref = window.location.search.match(/tsref=([^&]+)/)
				// if (tsref && tsref[1])
				// 	document.querySelector('.TS_GA_UTM_Referral_Code input').value = tsref[1] || ''

				const gclid = window.location.search.match(/gclid=([^&]+)/)
				if (gclid && gclid[1])
					document.querySelector('.TS_GA_ClickId__c input').value = gclid[1] || ''


				if (pageLang == 'de') {
					submitP.append('<span class="wait_message">Das kann einige Sekunden dauern.</span>')
				} else if (pageLang == 'fr') {
					submitP.append('<span class="wait_message">Cela peut prendre quelques secondes</span>')
				} else if (pageLang == 'ja') {
					submitP.append('<span class="wait_message">' + decodeURI('%E5%B0%91%E3%80%85%E3%81%8A%E5%BE%85%E3%81%A1%E3%81%8F%E3%81%A0%E3%81%95%E3%81%84%E3%80%82') + '</span>') // 少々お待ちください。
				} else {
					submitP.append('<span class="wait_message">This may take a few seconds.</span>')
				}

				let formClass = window.formClass || urlParams.get('formClass')
				if (formClass && typeof formClass != 'undefined') {
					form.addClass(formClass)
				}

				if (pageLang == 'de') {
					GDPRMessage = 'Ich bin damit einverstanden, dass ThoughtSpot mich zu Marketingzwecken per Telefon oder E-Mail kontaktiert. Dazu gehören Events, Whitepaper und andere Inhalte. Ich verstehe, dass ich mich jederzeit abmelden kann.'
					$(document).ready(function(){
						var span = $('.email span.description').first();
						if (span.length) {
							span.html(span.html().replace("Not","Nicht"));
							span.html(span.html().replace("Click Here","Klick hier"));
						}
					});
				} else if (pageLang == 'fr') {
					GDPRMessage = 'Vous acceptez que ThoughtSpot pourra utiliser vos données à caractère personnel à des fins de prospection, notamment pour vous informer sur la solution ThoughtSpot, sur les publications, ou sur les évènements. Vous pouvez vous désabonner à tout moment.'
					$(document).ready(function() {
						var span = $('.email span.description').first();
						if (span.length) {
							span.html(span.html().replace("Not","Pas"));
							span.html(span.html().replace("Click Here","Cliquez ici"));
						}
					});
				} else if (pageLang == 'ja') {
					// Encoded the japanese to solve garbling when uploading to pardot.
					// 私は、ThoughtSpotまたはそのパートナーが、マーケティング目的のため電話やEメールで連絡を取ることに同意します。私は、当該マーケティング情報の受け取りをいつでも停止できることを理解しています。
					GDPRMessage = decodeURI("%E7%A7%81%E3%81%AF%E3%80%81ThoughtSpot%E3%81%BE%E3%81%9F%E3%81%AF%E3%81%9D%E3%81%AE%E3%83%91%E3%83%BC%E3%83%88%E3%83%8A%E3%83%BC%E3%81%8C%E3%80%81%E3%83%9E%E3%83%BC%E3%82%B1%E3%83%86%E3%82%A3%E3%83%B3%E3%82%B0%E7%9B%AE%E7%9A%84%E3%81%AE%E3%81%9F%E3%82%81%E9%9B%BB%E8%A9%B1%E3%82%84E%E3%83%A1%E3%83%BC%E3%83%AB%E3%81%A7%E9%80%A3%E7%B5%A1%E3%82%92%E5%8F%96%E3%82%8B%E3%81%93%E3%81%A8%E3%81%AB%E5%90%8C%E6%84%8F%E3%81%97%E3%81%BE%E3%81%99%E3%80%82%E7%A7%81%E3%81%AF%E3%80%81%E5%BD%93%E8%A9%B2%E3%83%9E%E3%83%BC%E3%82%B1%E3%83%86%E3%82%A3%E3%83%B3%E3%82%B0%E6%83%85%E5%A0%B1%E3%81%AE%E5%8F%97%E3%81%91%E5%8F%96%E3%82%8A%E3%82%92%E3%81%84%E3%81%A4%E3%81%A7%E3%82%82%E5%81%9C%E6%AD%A2%E3%81%A7%E3%81%8D%E3%82%8B%E3%81%93%E3%81%A8%E3%82%92%E7%90%86%E8%A7%A3%E3%81%97%E3%81%A6%E3%81%84%E3%81%BE%E3%81%99%E3%80%82")

					$(document).ready(function() {
						var span = $('.email span.description').first();
						if (span.length) {
							span.html(span.html().replace("Not",""));
							span.html(span.html().replace("?",decodeURI("%20%E3%81%A7%E3%81%AA%E3%81%84%E5%A0%B4%E5%90%88%E3%81%AF%E3%80%81"))); // でない場合は、
							span.html(span.html().replace("Click Here",decodeURI("%E3%81%93%E3%81%A1%E3%82%89"))); // こちら
							span.html(span.html().replace(".",decodeURI("%E3%81%93%E3%81%A1%E3%82%89%E3%82%92%E3%82%AF%E3%83%AA%E3%83%83%E3%82%AF"))); // こちらをクリック
							try {
								var textNode = span.contents()[0]
								var stringArray = textNode.textContent.split(' ');
								stringArray = stringArray.filter(item => item);
								var nameArray = stringArray.slice(0, -1)
								var postNameString = stringArray.slice(-1)[0]
								var nameString = nameArray.reverse().join(' ')
								textNode.textContent = nameString + ' ' + postNameString
							} catch(err) {}
						}
					});
				}

				function markValidation(elem, validationStatus) {
					let error = elem.next('.error')
					let validationMessage = null

					if (validationStatus && validationStatus[0])
						validationStatus = validationStatus[0]

					if (validationStatus == "Please enter a non role-based email address") {
						validationMessage = validationStatus
						if (pageLang == 'ja') {
							validationMessage = decodeURI("%E5%BD%B9%E5%89%B2%E3%81%AB%E5%9F%BA%E3%81%A5%E3%81%8B%E3%81%AA%E3%81%84%E3%83%A1%E3%83%BC%E3%83%AB%E3%82%A2%E3%83%89%E3%83%AC%E3%82%B9%E3%82%92%E5%85%A5%E5%8A%9B%E3%81%97%E3%81%A6%E3%81%8F%E3%81%A0%E3%81%95%E3%81%84") // 役割に基づかないメールアドレスを入力してください
						} else if (pageLang == 'de') {
							validationMessage = 'Bitte geben Sie eine nicht rollenbasierte E-Mail-Adresse ein'
						} else if (pageLang == 'fr') {
							validationMessage = 'Veuillez entrer une adresse e-mail non basée sur le rôle'
						}
					}

					if (validationStatus == "can't be blank") {
						validationMessage = 'This field is required'
						if (pageLang == 'ja') {
							validationMessage = decodeURI("%E3%81%93%E3%81%AE%E9%A0%85%E7%9B%AE%E3%81%AF%E5%BF%85%E9%A0%88%E3%81%A7%E3%81%99%E3%80%82") // この項目は必須です。
						} else if (pageLang == 'de') {
							validationMessage = 'Dieses Feld wird benötigt'
						} else if (pageLang == 'fr') {
							validationMessage = 'Ce champ est requis'
						}
					}

					if (validationStatus == "Email address required") {
						validationMessage = validationStatus
						if (pageLang == 'ja') {
							validationMessage = decodeURI("%E3%83%A1%E3%83%BC%E3%83%AB%E3%82%A2%E3%83%89%E3%83%AC%E3%82%B9%E3%81%8C%E5%BF%85%E8%A6%81%E3%81%A7%E3%81%99") // メールアドレスが必要です
						} else if (pageLang == 'de') {
							validationMessage = 'Email addresse wird benötigt'
						} else if (pageLang == 'fr') {
							validationMessage = 'Adresse e-mail requise'
						}
					}

					if (validationStatus == "Business emails only") {
						validationMessage = validationStatus
						if (pageLang == 'ja') {
							validationMessage = decodeURI("%E5%8B%A4%E5%8B%99%E5%85%88%E3%81%AE%E3%83%A1%E3%83%BC%E3%83%AB%E3%82%A2%E3%83%89%E3%83%AC%E3%82%B9%E3%81%AE%E3%81%BF") // 勤務先のメールアドレスのみ
						} else if (pageLang == 'de') {
							validationMessage = 'Nur Firmenadressen bitte'
						} else if (pageLang == 'fr') {
							validationMessage = 'Email professionnel (uniquement)'
						}
					}

					if (validationStatus == "Invalid email") {
						validationMessage = 'Invalid email'
					}

					if (elem.is('[type="checkbox"]')) {
						if (validationStatus === true) {
							validationMessage = 'This field is required'
							elem.parent().addClass('has_error')
						} else {
							elem.parent().removeClass('has_error')
						}
					}

				
					if (elem.is('select') && elem.val() == elem.find('option:first-child').attr('value'))
						validationStatus = true


					if (validationStatus && elem.is(':visible')) {
						if (!error.length)
							error = $('<p class="error">' + validationMessage + '</p>')

						error.html(validationMessage)
						error.show()
						error.insertAfter(elem)
					} else {
						error.hide()
					}

					if (form.find('.error:visible').length) {
						disableSubmitButton()
					} else {
						console.log('enable submit button')
						enableSubmitButton()
					}
				}

				function checkAllValidation() {
					for (var i in validatedFields) {
						if (validatedFields[i] && validatedFields[i].trigger) {
							validatedFields[i].trigger('blur')
						}
					}
				}

				const fields = {
					first_name: '#First_Namepi_First_Name',
					last_name: '#Last_Namepi_Last_Name',
					email: '#Emailpi_Email',
					companyNumber: 'Work Phone',
					jobRole: 'Job Role',
					country: 'Select Country',
					state: 'Select State',
					industry: 'Industry',
					importTitle: '.Import_Title'
				}

				if (pageLang == 'ja') {
					fields.companyNumber = decodeURI("%E4%BC%9A%E7%A4%BE%E9%9B%BB%E8%A9%B1") // 会社電話
					fields.jobRole = decodeURI("%E5%BD%B9%E8%81%B7") // 役職
					fields.country = decodeURI("%E5%9B%BD%E3%82%92%E9%81%B8%E6%8A%9E") // 国を選択
					fields.state = decodeURI("%E5%B7%9E%E3%82%92%E9%81%B8%E6%8A%9E") // 州を選択
					fields.industry = decodeURI("%E6%A5%AD%E7%95%8C") // 業界
					fields.importTitle = '.Import_Title'
				} else if (pageLang == 'de') {
					fields.salutation = 'Anrede'
					fields.companyNumber = 'Geschäftliche Telefonnummer'
					fields.jobRole = 'Position auswählen'
					fields.country = 'Land auswählen'
					fields.state = 'Select State'
					fields.industry = 'Branche auswählen'
				} else if (pageLang == 'fr') {
					fields.salutation = 'Civilité'
					fields.companyNumber = 'Tél.'
					fields.jobRole = "Fonction"
					fields.country = 'Pays'
					fields.state = "État"
					fields.industry = 'Industrie'
				}

				const salutationField = form.find('label:contains("' + fields.salutation + '")').next()
				const salutationFieldWrapper = salutationField.parent('.form-field')

				const firstNameField = form.find(fields.first_name)
				const firstNameFieldWrapper = firstNameField.parent('.form-field')

				const lastNameField = form.find(fields.last_name)
				const lastNameFieldWrapper = lastNameField.parent('.form-field')

				const emailField = form.find(fields.email)
				const emailFieldWrapper = emailField.parent('.form-field')

				const phoneField = form.find('label:contains("Phone")').next()
				const phoneFieldWrapper = phoneField.parent('.form-field')

				const companyNumberField = form.find('label:contains("' + fields.companyNumber + '")').next()
				const companyNumberFieldWrapper = companyNumberField.parent('.form-field')

				const jobRoleField = form.find('label:contains("' + fields.jobRole + '")').next()
				const jobRoleWrapper = jobRoleField.parent('.form-field')

				const countryField = form.find('label:contains("' + fields.country + '")').next()
				const countryFieldWrapper = countryField.parent('.form-field')

				const stateField = form.find('label:contains("' + fields.state + '")').next()
				const stateFieldWrapper = stateField.parent('.form-field')

				const industryField = form.find('label:contains("' + fields.industry + '")').next()
				const industryFieldWrapper = industryField.parent('.form-field')
				industryFieldWrapper.hide()

				const gdprField = form.find('.GDPR_Consent_Checkbox input, .GDPR_Express_Consent input')
				const gdprFieldWrapper = form.find('.GDPR_Consent_Checkbox')

				const importTitleField = form.find(fields.importTitle + ' input')
				const importTitleFieldWrapper = importTitleField.parent('.form-field')

				const CloudExpressAdminAccessCheckbox = form.find('.CloudExpress_Has_AWS_Access input')
				const CloudExpressDataAccessCheckbox = form.find('.CloudExpress_Has_Data_Access input')

				const googleSignupField = form.find('.SaaS_Trial_Social_Signin input')			

				const saasTrialRoleField = form.find('.SaaS_Trial_Role input')
				const saasTermsAcceptedCheckbox = form.find('.SaaS_Trial_Terms_Accepted input')
				const saasDataWarehouseField = form.find('.SaaS_Trial_Data_Access select')
				const saasTrialApprovalStatusField = form.find('.SaaS_Trial_Approval_Status input')
				const saasTrialImportNotesField = form.find('.Import_Notes_2 input')
				const saasTrialUUIDField = form.find('.SaaS_Trial_UUID input')
				// This field is not named right. It's a re-used field that holds the Trial Role Field value.
				const saasYibField = form.find('.years_in_business input')

				const successRedirect = form.find('.success_redirect input')

				// console.log([firstNameField, lastNameField, emailField, companyNumberField, jobRoleField, countryField, stateField, industryField, gdprField, importTitleField])

				const validatedFields = [firstNameField, lastNameField, emailField, phoneField, companyField, companyNumberField, jobRoleField, countryField, stateField, importTitleField]

				const configureReferralMode = function() {
					console.log('configureReferralMode', urlParams.get('inviteeEmail'), window.location)
					if (urlParams.get('inviteeEmail')) {
						emailField.val(urlParams.get('inviteeEmail'))
						emailField.prop('disabled', 'disabled')
						emailField.attr('data-force-disabled', 1)
					}
				}

				configureReferralMode()

				if (window.cloudExpress) {
					validatedFields.push(CloudExpressAdminAccessCheckbox)
					validatedFields.push(CloudExpressDataAccessCheckbox)
				}

				if (window.trialForm) {
					validatedFields.push(saasTermsAcceptedCheckbox)
					validatedFields.push(saasDataWarehouseField)
				}

				if (salutationField.length)
					validatedFields.push(salutationField)

				function validateEmail(email) {
					if (validate.single(email, {presence: true, email: true})) {
						return ['Email address required'];
					}
					
					const splitEmail = email.split('@')
					let provider = splitEmail[1]

					if (window.businessEmailsOnly && (personalEmailsList.indexOf(provider) > -1 || personalEmailsList.indexOf(provider.toLowerCase()) > -1)) {
						return ['Business emails only'];
					}

					if (blackList.indexOf(provider) > -1 || blackList.indexOf(provider.toLowerCase()) > -1) {
						return ['Business emails only'];
					}

					let rejectForRole = roleEmailList.indexOf(splitEmail[0])

					if (rejectForRole > -1) {
						return ['Please enter a non role-based email address']
					}

					if (competitors.indexOf(provider) > -1 || competitors.indexOf(provider.toLowerCase()) > -1) {
						return ['Invalid email'];
					}

					return null;
				}

				function doValidation(elem, input) {
					if (elem == countryField[0])
						$('#invalid_country').hide()

					if (elem == emailField[0]) {
						markValidation(input, validateEmail(input.val()))
					} else if (input.is('select')) {
						let selectVal = input.val()
						if (selectVal == input.find('option:first-child').attr('value'))
							selectVal = ''
						markValidation(input, validate.single(selectVal, {presence: {allowEmpty: false}}))
					} else {
						markValidation(input, validate.single(input.val(), {presence: {allowEmpty: false}}))
					}
				}

				var keyupTimeout = null;
				for (var i in validatedFields) {
					const input = $(validatedFields[i]);

					if (!input.length)
						continue

					if (input.is('[type="checkbox"]') && (input.prop('required') || input.parents('.required').length)) {
						input.blur(function() {
							markValidation(input, !input.is(':checked'))
						}).click(function() {
							markValidation(input, !input.is(':checked'))
						})
					} else {
						input.blur(function() {
							doValidation(this, input)
						})

						if (input.is('input')) {
							input.keyup(function(e) {
								// e.key == 'Delete' || e.key == 'Backspace' ||
								if (e.key == 'Tab')
									return

								if (keyupTimeout)
									clearTimeout(keyupTimeout);

								keyupTimeout = setTimeout(function() {
									doValidation(input[0], input)
								}, 501);
							})
						}

						input.change(function(e) {
							doValidation(this, input)
						})
					}
				}

				// When they change the email hide this error. It will be shown again if they didn't follow instructions
				emailField.on('keyup, blur', function(e) {
					console.log('keyup, blur on emailfield')
					setTimeout(function() {
						$('#user_expired').hide()
						$('#user_already_active').hide()
						$('#user_authenticate_email').hide()
						enableSubmitButton()
					}, 500)
				})

				stateField.find('option').each(function() {
					const option = $(this)
					if (canadianProvinceNames.indexOf(option.text()) > -1) {
						canadianProvinces = canadianProvinces.add(option.remove())
					} else if (pageLang == 'ja') {
					} else if (option.text()) {
						usaStates = usaStates.add(option.remove())
					}
				})

				form.hide()

				const clearbitInterval = setInterval(function() {
					if (window.ClearbitForPardot) {
						clearInterval(clearbitInterval)
						const originalOnResult = window.ClearbitForPardot.onResult

						setTimeout(function() {
							form.show()
						}, 100)

						window.ClearbitForPardot.onResult = function(email, attributes) {
							form.show()
							originalOnResult(email, attributes)
							checkGDPR(attributes.Clearbit_HQ_Country, false)
						}
						window.ClearbitForPardot.initialize()
					}
				}, 100)

				setTimeout(function() {
					form.show()
				}, 500)

				// gdprField.change(function() {
				// 	console.log($(this).val())
				// })

				countryField.change(function() {
					countrySelected()
				})

				// companyField.blur(function() {
					// setTimeout(function() {
					// 	if (afterParseValue != companyField.val()) {
					// 		console.log('companyFieldBlur', afterParseValue != companyField.val(), afterParseValue, companyField.val())
					// 		afterParseValue = companyField.val()
					// 		clearDB();
					// 		showCountry();
					// 		countrySelected();
					// 	}
					// 	// console.log('companyField.blur', companyField.val(), afterParseValue)
					// }, 300)
				// })

				const hideCountry = function() {
					countryFieldWrapper.hide()
					stateFieldWrapper.hide()
					resizeIframe()
				}

				const showCountry = function() {
					if (!countryField.is(':visible'))
						countryField.val(countryField.find('option').eq(0).val())
					countryFieldWrapper.show()
					resizeIframe()
				}

				const countrySelected = function() {
					const country = countryField.find('option:selected').text()

					if (country == 'United States' || country == 'USA') {
						$(canadianProvinces).remove()
						stateField.append(usaStates)
						stateFieldWrapper.show()
						stateField.find('option').eq(0).text('Select State...')
					} else if (country == 'Canada' || country == 'Kanada') {
						$(usaStates).remove()
						stateField.append(canadianProvinces)
						stateFieldWrapper.show()
						stateField.find('option').eq(0).text('Select Province...')
					} else if (pageLang == 'ja' && country == decodeURI("%E6%97%A5%E6%9C%AC")) { // 日本
						stateFieldWrapper.show()
					} else {
						stateFieldWrapper.val('').hide()
					}

					checkGDPR(country)
					resizeIframe()
				}

				const checkGDPR = function(country, ip) {
					if (!ip && ipCountry) {
						country = ipCountry
					}

					if (!country) {
						return showGDPR()
					}

					if (gdprCountries.indexOf(country) > -1) {
						showGDPR()

						if (country == 'Japan')
							gdprField.prop("checked", true)
					} else {
						hideGDPR()
					}

					if (country == 'India' && companyNumberField && companyNumberField.length)
						companyNumberField.attr('placeholder', 'Mobile Phone')
				}

				const showGDPR = function() {
					// console.log('showGDPR')
					gdprFieldWrapper.show()
					// industryFieldWrapper.show()
					disableSubmitButton()
					resizeIframe()
				}

				const hideGDPR = function() {
					// console.log('hideGDPR')
					gdprFieldWrapper.hide()
					// industryFieldWrapper.hide()
					enableSubmitButton()
					resizeIframe()
				}

				const redirect = function(url) {
					window.parent.postMessage({
						event_id: 'redirect',
						data: {
							url: url
						}
					}, '*');
				}

				const checkForSubmitError = function() {
					let checkInterval = setInterval(function() {
						if (submitButton.attr('disabled')) {
							submitP.removeClass('show_loading')
							clearInterval(checkInterval)
						}
					}, 500)
				}

				const resizeIframe = function() {
					window.parent.postMessage({
						event_id: 'resize',
						data: {
							height: $('form').height(),
							url: window.location.href
						}
					}, '*');
				}

				function addPlaceholder() {
					const input = $(this)
					const label = input.prev()

					if (input.is('select')) {
						input.find('option:first-child').text(label.text() + '...')
					} else {
						input.attr('placeholder', label.text())
					}
				}

				function enableSubmitButton() {
					if (gdprField.is(':visible') && !gdprField.is(':checked')) {
						return
					}

					if (form.find('.error:visible').length) {
						return
					}

					submitButton.attr('disabled', false)
				}

				function disableSubmitButton() {
					submitButton.attr('disabled', true)
				}

				function disableForm() {
					form.find('input, select').prop('disabled', true)
				}

				function enableForm() {
					form.find('input, select').each(function() {
						if (!$(this).attr('data-force-disabled'))
							$(this).prop('disabled', false)
					})
				}

				function createUser(done) {
					const firstName = firstNameField.val()
					const lastName = lastNameField.val()
					const displayName = firstName + ' ' + lastName
					const email = emailField.val()
					const referral = urlParams.get('inviterId') ? true : false
					const altUrl = urlParams.get('altUrl') ? urlParams.get('altUrl') : (window.altUrl || null)
					const useOrgsClusters = `${urlParams.get('use_orgs_clusters')}` || window.useOrgsClusters || window.use_orgs_clusters || null
					const cluster = urlParams.get('cluster') || window.cluster || ''

					let name = email

					const dataWarehouse = saasDataWarehouseField ? saasDataWarehouseField.find('option:selected').text() : null

					if (dataWarehouse != 'Select Data Warehouse...') {
						let importNotes = 'DataWarehouse: ' + dataWarehouse
						saasTrialImportNotesField.val(importNotes)
					}

					let country = countryField.find('option:selected').is(':first-child') ? '' : countryField.find('option:selected').text()
					
					if (!country) {
						country = ipCountry
					}
					
					let corporateEmail = false

					// Check that email isn't from a list of blacklisted sources.
					const emailValue = emailField.val()


					// if (!referral) {
						// const emailDomain = emailValue.split('@')[1]
						// if (domainWhitelist.includes(emailDomain)) {
						// 	corporateEmail = true
						// }

					if (country && (countryBlacklist.includes(country) || countryBlacklist.includes(ipCountryCode))) {
						$('#invalid_country').show()
						console.log('Country blacklist fail', {country: country, ipCountry: ipCountry, ipCountryCode: ipCountryCode})
						return done(false, false)
					}

						// if (!corporateEmail && !referral) {
						// 	console.log('Corporate email whitelist fail')
						// 	return done(false, true)
						// }

						// if (corporateEmail && !(dataWarehouse == 'Snowflake' || dataWarehouse == 'AWS Redshift')) {
						// 	console.log('Corporate email whitelist pass but data warehouse fail.')
						// 	return done(false, true)
						// }

					if (window.trialFormForceManualReview === true)
						return done(false, true)
					// }

					var http_request = new XMLHttpRequest();

					function reqListener () {
						$('#system_error').hide()

						if (this.responseText) {
							try {

								const parsed = JSON.parse(this.responseText)

								console.log('api response', parsed.code)
								if (parsed.code == 409) {
									if (parsed.apiResponse.state == 'INACTIVE') {
										console.log('INACTIVE', $('#user_authenticate_email'))
										$('#user_authenticate_email').html(`Account exists. <a target="_blank" href="https://go.thoughtspot.com/resend-activation.html?email=${email}">Click here</a> to request an activation email.`)
										formErrorMixpanelMessage('Trial Form Submission Error', $('#user_authenticate_email').text())
										$('#user_authenticate_email').show()
										return done(false, false)
									} else if (parsed.apiResponse.state == 'EXPIRED') {
										console.log('EXPIRED', $('#user_expired'))
										$('#user_expired').show()
										formErrorMixpanelMessage('Trial Form Submission Error', $('#user_expired').text())
										return done(false, false)
									} else if (parsed.apiResponse.state == 'ACTIVE') {
										let stage = 'prod'
										try {
											const parsedStage = window.createUserUrl.match(/https:\/\/(\w+)\./)
											stage = parsedStage[1]

											const data = {
												email: email,
												partnerParams: ''
											}
									
											let signInUrl = `https://${stage || 'prod'}.tsfunctions.net/sign-in`
									
											fetch(signInUrl,  {
												method: 'POST',
												body: JSON.stringify(data),
												mode: 'cors',
												headers: {
													'Content-Type': 'application/json'
												}
											}).then(response => {
												response.json().then(responseJson => {
													if (responseJson.sent_email == 1) {
														$('#user_already_active').removeClass('error').html(`<div class=""><img style="margin-top: 25px; margin-bottom: 5px;" src="https://www.thoughtspot.com/images/check_circle.svg" width="31" /><p>If your account exists, please check your inbox for next steps.</p></div>`)
														formErrorMixpanelMessage('Trial Form Submission Error', $('#user_already_active').text())
														$('#user_already_active').show()
														return done(false, false)
													} else if (responseJson.principalTypeEnum == 'OIDC_USER') {
														const signInUrl = `${responseJson.domain}/callosum/v1/oidc/login?email_hint=${encodeURIComponent(responseJson.email)}`
														window.parent ? window.parent.location = signInUrl : window.location = signInUrl
													} else if (responseJson.domain) {
														const signInUrl = `${responseJson.domain}?email=${responseJson.email}`
														window.parent ? window.parent.location = signInUrl : window.location = signInUrl														
													} else {
														formErrorMixpanelMessage('Trial Form Submission Error', $('#user_already_active').text())														
														$('#user_already_active').show()
														return done(false, false)
													}
												})
											})

										} catch (e) {
											formErrorMixpanelMessage('Trial Form Submission Error', $('#user_already_active').text())
											return done(false, false)
										}
										return
									}
								} else if (parsed.code == 200) {
									if (parsed.apiResponse.header.id) {
										saasTrialUUIDField.val(parsed.apiResponse.header.id)

										// console.log('activationUrl', parsed.apiResponse.activationUrl)
										if (parsed.apiResponse.activationUrl && document.querySelector('.SaaS_Trial_Activation_URL input')) {
											let activationUrl = parsed.apiResponse.activationUrl.replace(/&tsref=[^&]+/, '')
											document.querySelector('.SaaS_Trial_Activation_URL input').value = activationUrl
										}

										if (parsed.apiResponse.serverName && document.querySelector('.SaaS_Trial_Cluster_URL input')) {
											document.querySelector('.SaaS_Trial_Cluster_URL input').value = parsed.apiResponse.serverName
										}

										return done(true, false)
									}
								} else if (parsed.code == 400) {
									formErrorMixpanelMessage('Trial Form Submission Error', $('#invalid_referral').text())
									$('#invalid_referral').show()
									return done(false, false)
								}

							} catch (error) {
								console.log('reqListener', error);
							}
						}

						formErrorMixpanelMessage('Trial Form Submission Error', $('#system_error').text())
						$('#system_error').show()
						configureReferralMode()
						return done(false, false)
					}

					// #dev
					// reqListener.bind({responseText: JSON.stringify({code: 409, apiResponse: {state: 'INACTIVE'}})})()
					// console.log("TEST")
					// reqListener.bind({responseText: JSON.stringify({code: 200, apiResponse: {state: 'ACTIVE'}})})()
					// return

					// let persona = 'other'
					let persona = 'analyst'
					let personaMap = {'Data Analyst': 'analyst', 'Business User': 'business_user'}
					// let personaValue = saasTrialRoleField.find('option:selected').text()
					// if (personaMap[personaValue])
					// 	persona = personaMap[personaValue]

					// When testing we want the name to not equal the email so you can create multiple accounts using the same email
					if (window.testing)
						name = firstName + '_' + lastName

					let source = 'web_free_trial'
					if (urlParams.get('partner') == 'redshift') {
						source = 'REDSHIFT_PARTNER_CONNECT'
					}

					// console.log('groupNames', $('[data-group-names]').html() || urlParams.get('groupNames') || '')

					try {
						if (email.indexOf('test14') > -1) {
							window.trialDuration = 14
							console.log('set trial duration to 14 days')
						}
					} catch (e) {
					}

					const requestData = JSON.stringify({
						name: name,
						displayName: displayName,
						groupNames: $('[data-group-names]').html() || urlParams.get('groupNames') || '',
						email: email,
						period: window.trialDuration || 14,
						source: source,
						persona: persona,
						country: country,
						ipCountry: ipCountry,
						inviterId: urlParams.get('inviterId'),
						inviteeEmail: urlParams.get('inviteeEmail'),
						companyName: companyField.val(),
						dataWarehouse: dataWarehouse || '',
						activatedUser: googleSignup || false,
						tsref: document.querySelector('.TS_GA_UTM_Referral_Code input').value || '',
						partnerParams: partnerParams ? partnerParams : ''
					})

					const lang = window.location.search.match(/lang=(.+?)&/)
					const params = []
					let langParam = ''
					if (lang && lang[1]) {
						langParam = 'lang=' + lang[1]
						params.push(langParam)
					}

					if (!langParam && pageLang) {
						langParam = 'lang=' + pageLang
						params.push(langParam)
					}

					let altUrlParam = ''
					if (altUrl) {
						altUrlParam = 'alt_url=' + altUrl
						params.push(altUrlParam)
					}

					if (useOrgsClusters || useOrgsClusters === 0) {
						params.push('use_orgs_clusters=' + useOrgsClusters)
					}

					if (cluster) {
						params.push('cluster=' + cluster)
					}

					const createUserUrl = window.createUserUrl || `https://next.thoughtspot.us`
					const createUserPath = window.createUserPath || '/api/create_user'
					if (['https://next.thoughtspot.us'].indexOf(createUserUrl) > -1) {
						params.push(`data=${encodeURIComponent(requestData)}`)
						http_request.addEventListener("load", reqListener);
						http_request.open("GET", `${createUserUrl}${createUserPath}${params.length ? '?' + params.join('&') : ''}`, true);
						http_request.withCredentials = false;
						http_request.setRequestHeader("Content-Type", "application/json");
						http_request.send({});
					} else {
						http_request.addEventListener("load", reqListener);
						http_request.open("POST", `${createUserUrl}${createUserPath}${params.length ? '?' + params.join('&') : ''}`, true);
						http_request.withCredentials = false;
						http_request.setRequestHeader("Content-Type", "application/json");
						http_request.send(requestData);
					}
					form.find('input, select').prop('disabled', true)

					// Test without making request
					// reqListener.call({responseText: '{"code": 409, "apiResponse": {"userContent":{"userPreferences":{"showWalkMe":true,"notifyOnShare":true},"userProperties":{"mail":"jorenm@gmail.com","source":"FREE_TRIAL"},"userActivityProto":{"first_login":-1,"welcome_email_sent":false}},"state":"INACTIVE","expiry":1581901965027,"assignedGroups":["6b888d7b-9410-42b5-a399-3cf6c42e4833","b25ee394-9d13-49e3-9385-cd97f5b253b4"],"inheritedGroups":["6b888d7b-9410-42b5-a399-3cf6c42e4833","b25ee394-9d13-49e3-9385-cd97f5b253b4"],"privileges":["A3ANALYSIS","DATADOWNLOADING","USERDATAUPLOADING","DATAMANAGEMENT","AUTHORING"],"type":"LOCAL_USER","parenttype":"USER","visibility":"DEFAULT","tenantId":"982d6da9-9cd1-479e-b9a6-35aa05f9282a","displayName":"asdf Mathews","header":{"id":"13e9e75c-3925-4dab-88a2-9d81d38de4df","indexVersion":14387,"generationNum":14387,"name":"jorenm@gmail.com","displayName":"asdf Mathews","author":"59481331-ee53-42be-a548-bd87be6ddd4a","created":1579309965028,"modified":1579309965028,"modifiedBy":"59481331-ee53-42be-a548-bd87be6ddd4a","owner":"13e9e75c-3925-4dab-88a2-9d81d38de4df","isDeleted":false,"isHidden":false,"tags":[],"type":"LOCAL_USER","isExternal":false},"complete":true,"incompleteDetail":[],"isSuperUser":false,"isSystemPrincipal":false}}'})
				}

				function createCloudExpressKey(done) {
					const email = emailField.val()

					var http_request = new XMLHttpRequest();

					function reqListener () {
						if (this.responseText) {
							const parsed = JSON.parse(this.responseText)
							// console.log('api response', parsed)
							return done(parsed);
						}
						return done(false)
					}

					const lang = window.location.search.match(/lang=(.+)&/)
					let langParam = ''
					if (lang && lang[1])
						langParam = '&lang=' + lang[1]
					if (!langParam && pageLang)
						langParam = '&lang=' + pageLang

					http_request.addEventListener("load", reqListener);
					// http_request.open("GET", `https://cloudexp-thoughtspot.pantheonsite.io/pardot/generate-license-key?email=${email}`, true);
					// http_request.open("GET", `https://user-api-thoughtspot.pantheonsite.io/pardot/generate-license-key?email=${email}${langParam}`, true);
					http_request.open("GET", `https://www.thoughtspot.com/pardot/generate-license-key?email=${email}${langParam}`, true);
					http_request.withCredentials = false;
					http_request.setRequestHeader("Content-Type", "application/json");
					http_request.send({});
				}

				inputFields.each(addPlaceholder)
				selectFields.each(addPlaceholder)

				gdprFieldWrapper.hide()
				industryFieldWrapper.hide()
				countryFieldWrapper.hide()
				stateFieldWrapper.hide()

				gdprField.on('change', function() {
					if (gdprField.is(':checked')) {
						enableSubmitButton()
					} else {
						disableSubmitButton()
					}
				})


				let skipSubmitLogic = false
				window.formWillSubmit = false
				form.on('submit', function() {
					if (skipSubmitLogic) {
						skipSubmitLogic = false
						window.formWillSubmit = true

						// try {
						// 	if (window.location.search.indexOf('metadata_cid') > -1) {
						// 		const formValues = Object.fromEntries(new FormData(form[0]));
						// 		console.log('Test Metadata Values', formValues)
						// 		window.Metadata.siteScript.sendData(formValues)
						// 	}
						// } catch (e) {
						// 	console.log('submitToPardot Metadata fail', e)
						// }

						return true
					}

					disableForm()
					try {
						const errors = [...document.querySelectorAll('.pardot_embed_message.error')]
						errors.map((error) => {
							error.style.display = 'none'
						})
					} catch (e) {
						console.log('e')
					}

					if (urlParams.get('partner') && !partnerParams) {
						return false
					}

					// saasYibField.val(saasTrialRoleField.find('option:selected').text())
					saasYibField.val('analyst')
					saasTrialRoleField.val('analyst') // This is temporary until this field is made a select again.

					formSubmittedMixpanelMessage('Trial Form Submit Button Clicked')

					checkAllValidation()

					if (submitButton.attr('disabled')) {
						console.log('Aborting submit. Invalid values.')
						enableForm()
						disableSubmitButton()
						return false
					}

					submitP.addClass('show_loading')

					if (window.trialForm) {

						// Test for dev
						// setTimeout(function() {
						// 	submitP.removeClass('show_loading')
						// 	enableForm()
						// }, 4000)
						// return false

						createUser(function(created, submitForm) {
							console.log('created', created, submitForm)

							form.find('input, select').each((index, elem) => {
								if (!$(elem).attr('data-force-disabled')) {
									$(elem).prop('disabled', false)
								}
							})

							if (created) {
								skipSubmitLogic = true
								saasTrialApprovalStatusField.val('auto')
								successRedirect.val(successRedirect.val() + '?createdUser=true' + (googleSignup ? '&googleSignup=true' : ''))
								console.log(successRedirect.val())
								// How many submitted the form? Signup-button-clicked. Include form info (Campaign, Email, Company, job title, and CDW) as event property

								formSubmittedMixpanelMessage('Trial Form Submitted')
								form.submit()
							} else {
								if (submitForm) {
									skipSubmitLogic = true
									saasTrialApprovalStatusField.val('manual')
									successRedirect.val(successRedirect.val() + '?createdUser=false')
									formSubmittedMixpanelMessage('Trial Form Submitted')
									form.submit()
								} else {
									submitP.removeClass('show_loading')
								}
							}
						})
						return false
					}

					if (window.cloudExpress) {
						createCloudExpressKey(function(created) {
							submitP.removeClass('show_loading')
							if (created) {
								form.find('.CloudExpress_License input').val(created.key)
								form.find('.CloudExpress_License_URL input').val(created.key_link)
								form.find('.UUID input').val(created.uuid)
								skipSubmitLogic = true
								form.submit()
							} else {
								$('#failed_key_creation').show()
								enableForm()
								setTimeout(function() {
									$('#failed_key_creation').fadeOut()
								}, 4000)
							}
						})
						return false;
					}

					// This is so the submit spinner goes away if pardot stops the form submit.
					checkForSubmitError()
				})

				$(window).on('load', function() {
					setTimeout(function() {
						resizeIframe()
					}, 1000)
				});

				setInterval(resizeIframe, 1000)

				// Test
				// $(window).on('keyup', function(e) {
				// 	if (e.key == ',') {
				// 		db_noSourceMatch()
				// 	} else if (e.key == '.') {
				// 		db_afterParse_hook({country_name: 'Ireland'}, 'company')
				// 	}
				// });

				let matched = document.location.search.match(/style=(\w+)/)
				if (matched && matched[1])
					form.addClass('style_' + matched[1])

				form.addClass('prepared')

				function geolocate() {
					var http_request = new XMLHttpRequest();
					function reqListener () {
						console.log('geolocate', this.responseText)
						if (this.responseText) {
							let parsed = {}
							try {
								parsed = JSON.parse(this.responseText)
							} catch (e) {}

							if (parsed.state)
								form.find('.Inferred_State input').val(parsed.state)

							if (parsed.country) {
								form.find('.Inferred_Country input').val(parsed.country)
								if (!ipCountry)
									ipCountry = parsed.country
								if (!ipCountryCode)
									ipCountryCode = parsed.country_code
							}

							if (parsed.zip)
								form.find('.Inferred_Zip_Code input').val(parsed.zip)

							if (parsed.city)
								form.find('.Inferred_City input').val(parsed.city)

							checkGDPR(parsed.country, 'ip')
						}
					}

					http_request.addEventListener("load", reqListener);
					http_request.open("GET", "https://prod.tsfunctions.net/geolocate");
					// http_request.open("GET", "https://thoughtspotnext.ngrok.io/api/geolocate");
					http_request.withCredentials = false;
					http_request.setRequestHeader("Content-Type", "application/json");
					http_request.send({});
				}

				geolocate()

				disableSubmitButton()

				$(window).on("message", function(e) {
					const message = e.originalEvent.data

					// console.log('iframe received message', message)
					if (message && message.event_id == 'passthrough_arg' && message.data.argname && message.data.argvalue) {
						$('.' + message.data.argname + ' input').val(message.data.argvalue)
						// console.log('set arg', '.' + message.data.argname + ' input', $('.' + message.data.argname + ' input'));
					}

					if (message && message.event_id == 'geolocated') {
						if (message.data) {
							const parsed = JSON.parse(message.data)
							if (parsed.state)
								form.find('.Inferred_State input').val(parsed.state)
							if (parsed.country)
								form.find('.Inferred_Country input').val(parsed.country)
							if (parsed.city)
								form.find('.Inferred_City input').val(parsed.city)
						}
					}
				});

				$('.GDPR_Consent_Checkbox').each(function() {
					$(this).find('label').text(GDPRMessage);
				});

				// I'm mapping placeholder back onto label since the code was originally written to customize placeholder because that's what we were displaying.
				inputFields.each(function() {
					$(this).prev().html($(this).attr('placeholder'))
					if ($(this).val())
						$(this).prev().addClass('populated')
				})

				inputFields.on('focus', function() {
					$(this).prev().addClass('focused')
				}).on('blur', function() {
					$(this).prev().removeClass('focused')
					if ($(this).val()) {
						$(this).prev().addClass('populated')
					} else {
						$(this).prev().removeClass('populated')
					}
				})

				selectFields.on('change', function() {
					if ($(this).val() && $(this).val() != $(this).find('option').eq(0).attr('value')) {
						$(this).addClass('populated').prev().addClass('populated')
					} else {
						$(this).removeClass('populated').prev().removeClass('populated')
					}
				}).trigger('change')

				function initializeGoogleSignup() {

					// Create user api url has query param that tells system the user is already activated.
					// ?activate=true
			
					// populate and lock the name/email fields with the response from google.
			
					// Add property to create user api call for google authenticated users 
					// principalTypeEnum”:“OIDC_USER” (allows us to identify users who used Google sign up)
			
					// To get redirect url so that the user can go direct to the product after form submission “https://<host>/callosum/v1/oidc/login”

					const popupCenter = ({url, title, w, h}) => {
						function FindLeftWindowBoundry() {
							// In Internet Explorer window.screenLeft is the window's left boundry
							if (window.screenLeft) {
								return window.screenLeft
							}

							// In Firefox window.screenX is the window's left boundry
							if (window.screenX) {
								return window.screenX
							}

							return 0;
						}
						// Find Left Boundry of current Window
						function FindTopWindowBoundry() {
							// In Internet Explorer window.screenLeft is the window's left boundry
							if (window.screenTop) {
								return window.screenTop
							}

							// In Firefox window.screenY is the window's left boundry
							if (window.screenY) {
								return window.screenY
							}

							return 0;
						}

						var x = screen.width/2 - w/2 + FindLeftWindowBoundry()
						var y = screen.height/2 - h/2 + FindTopWindowBoundry()
						return window.open(url, title, `width=${w},height=${h},left=${x},top=${y},status=0,toolbar=0,location=0`);
					}
			
					const parseJwt = (token) => {
						try {
							return JSON.parse(atob(token.split('.')[1]))
						} catch (e) {
							return null
						}
					}
			
					function handleProfileResponse(profile) {
						// console.log('handleProfileResponse', JSON.stringify(profile))
						googleSignup = true
						if (googleSignupField) {
							googleSignupField.val('Google')
						}

						const email = profile.email
						const firstName = profile.given_name
						const lastName = profile.family_name
						
						emailField.val(email)
						const emailLabel = document.querySelector('.email label')

						divider.style.display = 'none'
						// additionalInfo.style.display = 'block'

						// Null is returned if there are no issues.
						if (validateEmail(email) === null) {
							emailLabel.classList.add('disabled')
							emailField.prop('disabled', true)
							doValidation(emailField[0], emailField)
						} else {
							emailLabel.classList.remove('disabled')
							emailField.prop('disabled', false)
							doValidation(emailField[0], emailField)
							googleSignup = false
						}

						// Trigger input event so Clearbit runs.
						const event = new Event('input')
						emailField[0].dispatchEvent(event)

						emailField.prev().addClass('populated')
						emailFieldWrapper.find('.description').hide()

						firstNameField.val(firstName)
						lastNameField.val(lastName)
						form.find('.First_Name_Hidden input').val(firstName)
						form.find('.Last_Name_Hidden input').val(lastName)

						firstNameFieldWrapper.addClass('hide')
						lastNameFieldWrapper.addClass('hide')
						companyField.parent().addClass('hide')
						companyNumberFieldWrapper.addClass('hide')
						importTitleFieldWrapper.addClass('hide')
						saasTermsAcceptedCheckbox.parent().addClass('hide')

						if (companyNumberField) {
							companyNumberField.remove()
						}

						if (saasTermsAcceptedCheckbox) {
							saasTermsAcceptedCheckbox.remove()
						}

						if (saasDataWarehouseField) {
							saasDataWarehouseField.parent().addClass('hide')
							saasDataWarehouseField.remove()
						}

						if (!gdprField.is(':visible')) {
							// Fix the google signup failing because of the email field blur listener timeout.
							setTimeout(() => {
								form.trigger('submit')
							}, 600)
						} else {
							const gdprErrorMessage = document.createElement('div')
							gdprErrorMessage.classList.add('gdpr_error')
							gdprErrorMessage.textContent = 'Must check consent box.'
							gdprFieldWrapper[0].appendChild(gdprErrorMessage)
							gdprField.on('change', function() {
								if (gdprField.is(':checked')) {
									gdprErrorMessage.remove()
									form.trigger('submit')
								}
							})
						}
					}

					let divider = document.createElement('div')
					divider.innerHTML = '<span></span> OR <span></span>'
					divider.id = 'form_divider'

					let additionalInfo = document.createElement('div')
					additionalInfo.innerHTML = 'Please provide additional info:'
					additionalInfo.id = 'form_additional_info'
					additionalInfo.style.display = 'none'

					const googleScript = document.createElement('script')
					googleScript.src = 'https://accounts.google.com/gsi/client'
					// ?hl=' + ({ja: 'ja_JP', de: 'de_DE', fr: 'fr_FR', en: 'en_US'}[pageLang] || 'en_US')
					document.body.appendChild(googleScript)

					function googleReady() {
						const buttonWrapper = document.createElement('div')
						buttonWrapper.id = 'googleSignupDiv'
						buttonWrapper.style.opacity = '0'

						if (document.querySelector('#google_wrapper')) {
							document.querySelector('#google_wrapper').prepend(divider)
							document.querySelector('#google_wrapper').prepend(additionalInfo)
							document.querySelector('#google_wrapper').prepend(buttonWrapper)

						} else {
							document.querySelector('#pardot-form').prepend(divider)
							document.querySelector('#pardot-form').prepend(additionalInfo)
							document.querySelector('#pardot-form').prepend(buttonWrapper)
						}


						let buttonInitialized = false
						function createOverlayButton() {
							buttonInitialized = true
							const button = document.createElement('button')
							button.classList.add('google_signup_button')
							button.innerHTML = 'Sign up with Google'

							if (pageLang == 'de') {
								button.innerHTML = 'Melden Sie sich bei Google an'
							} else if (pageLang == 'fr') {
								button.innerHTML = 'S\'inscrire avec Google'
							} else if (pageLang == 'ja') {
								// button.innerHTML = 'Google' + decodeURI('%E3%82%A2%E3%82%AB%E3%82%A6%E3%83%B3%E3%83%88%E3%81%A7') // アカウントで登録
								button.innerHTML = 'Google' + decodeURI('%E3%82%A2%E3%82%AB%E3%82%A6%E3%83%B3%E3%83%88%E3%81%A7%E7%99%BB%E9%8C%B2') // アカウントで
							} else {
								button.innerHTML = 'Sign up with Google'
							}

							buttonWrapper.appendChild(button)
						}

						google.accounts.id.initialize({
							// client_id: '69555622403-ljaq2t897l6tva08du06rnbphbmc8p50.apps.googleusercontent.com',
							client_id: '890995717819-ii3jdte5p3bkt834i9v1k0bj1m5e928j.apps.googleusercontent.com',
							callback: (response) => {
								const user = parseJwt(response.credential)
								handleProfileResponse(user)
							}
						})

						function renderButton(width) {
							google.accounts.id.renderButton(
								document.getElementById("googleSignupDiv"), {
									theme: 'outline',
									size: 'large',
									width: width,
									text: 'signup_with'
								}
							)
						}

						// We'll try both string and number, because we're seeing inconsistencies in what it expects.
						try {
							renderButton(272)
						} catch (e) {
							console.log('renderButton exception', e)
							renderButton('272')
						}

						createOverlayButton()
						setTimeout(() => {
							buttonWrapper.style.opacity = 1
						}, 500)
					}

					function awaitGoogle() {
						if (typeof google != 'undefined') {
							clearInterval(awaitGoogleInterval)
							googleReady()
						}
					}

					const awaitGoogleInterval = setInterval(awaitGoogle, 200)
				}
				
				// ?accountId=982380167162&region=us-east-1&clusterUrl=redshift-embrace-test.che06oeie0t3.us-east-1.redshift.amazonaws.com&dbPort=5439&dbName=dev&username=admin
				function initializeRedshift() {
					const redshiftOption = saasDataWarehouseField.find('option:contains("Amazon Redshift")')

					saasDataWarehouseField[0].value = redshiftOption.prop('value')
					saasDataWarehouseField[0].disabled = true
					saasDataWarehouseField[0].setAttribute('data-force-disabled', true)

					partnerParams = {
						accountId: urlParams.get('accountId'),
						region: urlParams.get('region'),
						clusterUrl: urlParams.get('clusterUrl'),
						dbPort: urlParams.get('dbPort'),
						dbName: urlParams.get('dbName'),
						username: urlParams.get('username'),
					}

					const queryString = new URLSearchParams(partnerParams).toString()

					const alreadyHaveAccount =  $('.already_have_account')
					if (alreadyHaveAccount.length) {
						const a = alreadyHaveAccount.find('a')
						let url = a.prop('href')
						if (url.indexOf('?') > -1) {
							url += '&' + queryString
						} else {
							url += '?' + queryString
						}
						a.prop('href', url)
					}

					const loginLink = $('#user_already_active a').prop('href')

					if (loginLink.indexOf('?') > -1) {
						$('#user_already_active a').prop('href', loginLink + '&' + queryString)
					} else {
						$('#user_already_active a').prop('href', loginLink + '?' + queryString)
					}
					
				}

				if (window.googleSignin) {
					initializeGoogleSignup()
				}

				console.log('partner', urlParams.get('partner'), window.location.search)

				if (urlParams.get('partner') == 'redshift') {
					initializeRedshift()
				}

				// #dev autofill
				// console.log('autofill')
				// firstNameField.val('Joren')
				// lastNameField.val('Mathews')
				// emailField.val('loremipsum99@mailinator.com')
				// companyField.val('3fo')
				// companyNumberField.val('4085883222')
				// jobRoleField.val('693143') // Developer
				// saasTrialRoleField.val('693773') // Data analyst
				// // countryField.val('693151') // USA
				// // stateField.val('693667') // Arizona
				// saasTermsAcceptedCheckbox.prop('checked', 'checked')

			});

		})();
	}
}