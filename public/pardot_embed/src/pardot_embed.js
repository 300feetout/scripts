// Updated 7/2/2021

import 'url-search-params-polyfill'
import { datadogLogs } from '@datadog/browser-logs'
import validate from 'validate.js'

import dayjs from 'dayjs'

const utc = require('dayjs/plugin/utc')
const timezone = require('dayjs/plugin/timezone')
dayjs.extend(utc)
dayjs.extend(timezone)

const blackList = require('./email_blacklist.js')
const personalEmails = require('./personal_emails.js')
const competitors = require('./competitor_blacklist.js')
const roleEmailList = require('./email_role_blacklist.js')

import jQuery from "jquery"
window.$ = window.jQuery = jQuery

var urlParams = new URLSearchParams(window.location.search);

datadogLogs.init({
	clientToken: 'pubb6c0118727d12d9601eff275e37ac919',
	site: 'us3.datadoghq.com',
	service: 'pardot_embed.js',
	forwardErrorsToLogs: true,
	trackingConsent: 'not-granted',
	sampleRate: 100,
})

if (!window.pardotEmbedInitialized) {
	if (!window.loadedLocalScript && window.location.search.indexOf('use_local_script=1') > -1) {

		console.log('loading_local_script')
		window.loadedLocalScript = true
		const localScript = document.createElement('script')
		localScript.src = 'https://thoughtspotscripts.ngrok.io/public/pardot_embed/dist/pardot_embed.bundled.js'
		document.body.appendChild(localScript)

	} else {

		window.pardotEmbedInitialized = true
		require('./pardot_embed.styl');

		const WebFont = require('../node_modules/webfontloader/webfontloader.js');

		WebFont.load({
			custom: {
				families: ['Optimo-Plain', 'BB Roller Mono'],
				urls: ['https://thoughtspot.com/sites/all/themes/thoughtspot_new/fonts/fonts.css']
			},
		});

		(function() {
			let GDPRMessage = 'I consent to ThoughtSpot, or its partners, contacting me via phone or email for marketing purposes. These include events, white papers, and other content. I understand that I can opt out at any time.';

			pardot.$(document).ready(function() {
				// Used for GDPR
				const countryName = ["Norway","Japan","Austria","Italy","Belgium","Latvia","Bulgaria","Lithuania","Croatia","Luxembourg","Cyprus","Malta","Czech","Republic","Netherlands","Denmark","Poland","Estonia","Portugal","Finland","Romania","France","Slovakia","Germany","Slovenia","Greece","Spain","Hungary","Sweden","Ireland","United Kingdom", "Australia", "New Zealand", "Singapore"]

				const countryShortName = ["BE", "BG", "CZ", "DK", "DE", "EE", "IE", "EL", "ES", "FR", "HR", "IT", "CY","LV", "LT", "LU", "HU", "MT", "NL", "AT", "PL", "PT", "RO", "SI", "SK", "FI","SE", "GB"]
				const canadianProvinceNames = ['Alberta', 'British Columbia', 'Manitoba', 'New Brunswick', 'Newfoundland', 'Nova Scotia', 'Nunavut', 'Ontario', 'Prince Edward Island', 'Quebec', 'Saskatchewan', 'Yukon Territory', 'Northwest Territories']
				let canadianProvinces = $('')
				let usaStates = $('')

				let afterParseValue = ''
				let afterParseCountry = ''
				let ipCountry = ''
				let gdprIPMatch = false

				const form = $('form#pardot-form')
				const submitP = form.find('p.submit')
				const selectFields = form.find('.pd-select select')
				const inputFields = form.find('.pd-text input, .pd-textarea textarea')

				const saasDataWarehouseField = null

				var pageLang = $('html').attr('lang') || window.formLang

				let gclidValue = window.location.search.match(/gclid=([^&]+)/)
				if (gclidValue && gclidValue[1]) {
					form.find('.TS_GA_ClickId__c input').val(gclidValue[1])
				}

				let additionalMixpanelParams = window.location.search.match(/mixpanelProps=([^&]+)/)
				let additionalMixpanelParamsObject = {}
				if (additionalMixpanelParams && additionalMixpanelParams[1]) {
					try {
						additionalMixpanelParams = decodeURIComponent(additionalMixpanelParams[1])
						additionalMixpanelParams = additionalMixpanelParams.split(',').map((paramGroup) => {
							const paramGroupSplit = paramGroup.split(':')
							additionalMixpanelParamsObject[paramGroupSplit[0]] = paramGroupSplit[1]
						})
						
					} catch(e) {
						console.log('Failed to parse params', e)
					}
				}

				const sendMixpanelEvent = function(message, data) {
		
					Object.assign(data, additionalMixpanelParamsObject)

					if (typeof mixpanel == 'object' && mixpanel.track) {
						mixpanel.track(message, data)
					} else if (window.parent) {
						if (message)
							data.mixpanelEvent = message

						window.parent.postMessage({
							event_id: 'mixpanel',
							data: data
						}, '*')
					}

				}

				form.find(".TS_URL input").val(window.location.origin + window.location.pathname)

				const mixpanelEvent = function(e, message, data) {
					if (e) {
						const formElem = $(e.target)

						sendMixpanelEvent(message, {
							type: e.type,
							id: formElem.attr('id'),
							name: formElem.is('select') ? formElem.find('option').eq(0).text() : formElem.attr('placeholder'),
							value: formElem.is('select') ? formElem.find('option:selected').text() : formElem.val(),
						})
					} else {
						sendMixpanelEvent(message, data)
					}
				}

				const formSubmittedMixpanelMessage = function(message) {
					mixpanelEvent(null, message, {
						email: emailField.val(),
						company: companyField.val(),
						jobTitle: jobRoleField.find('option:selected').text(),
						cdw: saasDataWarehouseField ? saasDataWarehouseField.find('option:selected').text() : '',
						tsref: document.querySelector('.TS_GA_UTM_Referral_Code input').value
					})
				}

				const formErrorMixpanelMessage = function(message, error) {
					mixpanelEvent(null, message, {
						userVisibleError: error,
						email: emailField.val(),
						company: companyField.val(),
						jobTitle: jobRoleField.find('option:selected').text(),
						cdw: saasDataWarehouseField ? saasDataWarehouseField.find('option:selected').text() : '',
						tsref: document.querySelector('.TS_GA_UTM_Referral_Code input').value
					})
				}

				let delayListeners = true
				setTimeout(() => {
					delayListeners = false
				}, 1000)

				// Track interactions for mixpanel
				if (inputFields) {
					inputFields.each(function() {
						$(this).on('focus', (e) => {
							if (!delayListeners) {
								mixpanelEvent(e, 'Trial Pardot Form Interaction')
							}
						})
					})
					inputFields.each(function() {
						$(this).on('change', (e) => {
							if (!delayListeners) {
								mixpanelEvent(e, 'Trial Pardot Form Interaction')
							}
						})
					})
				}
				if (selectFields) {
					selectFields.each(function() {
						$(this).on('focus', (e) => {
							if (!delayListeners) {
								mixpanelEvent(e, 'Trial Pardot Form Interaction')
							}
						})
					})
					selectFields.each(function() {
						$(this).on('change', (e) => {
							if (!delayListeners) {
								mixpanelEvent(e, 'Trial Pardot Form Interaction')
							}
						})
					})
				}

				if (pageLang == 'de') {
					submitP.append('<span class="wait_message">Das kann einige Sekunden dauern.</span>')
				} else if (pageLang == 'fr') {
					submitP.append('<span class="wait_message">Cela peut prendre quelques secondes</span>')
				} else if (pageLang == 'ja') {
					submitP.append('<span class="wait_message">' + decodeURI('%E5%B0%91%E3%80%85%E3%81%8A%E5%BE%85%E3%81%A1%E3%81%8F%E3%81%A0%E3%81%95%E3%81%84%E3%80%82') + '</span>') // 少々お待ちください。
				} else {
					submitP.append('<span class="wait_message">This may take a few seconds.</span>')
				}

				const submitButton = form.find('input[type="submit"]')
				const companyField = form.find('#Companypi_Company')

				const var3input = form.find('.Variable_3 input')
				var3input.val('pardot loaded')

				const var2input = form.find('.Variable_2 input')
				if (var2input) {
					var2input.val(dayjs.utc().tz('America/Los_Angeles').format('YYYY-MM-DD'))
				}

				let formClass = window.formClass || urlParams.get('formClass')
				console.log('formClass', window.formClass, urlParams.get('formClass'))
				if (formClass && typeof formClass != 'undefined') {
					form.addClass(formClass)
				}

				if (pageLang == 'de') {
					GDPRMessage = 'Ich bin damit einverstanden, dass ThoughtSpot mich zu Marketingzwecken per Telefon oder E-Mail kontaktiert. Dazu gehören Events, Whitepaper und andere Inhalte. Ich verstehe, dass ich mich jederzeit abmelden kann.'

					$(document).ready(function(){
						var span = $('.email span.description').first();
						if (span.length) {
							span.html(span.html().replace("Not","Nicht"));
							span.html(span.html().replace("Click Here","Klick hier"));
						}
					});
				} else if (pageLang == 'fr') {
					GDPRMessage = 'Vous acceptez que ThoughtSpot pourra utiliser vos données à caractère personnel à des fins de prospection, notamment pour vous informer sur la solution ThoughtSpot, sur les publications, ou sur les évènements. Vous pouvez vous désabonner à tout moment.'
					$(document).ready(function() {
						var span = $('.email span.description').first();
						if (span.length) {
							span.html(span.html().replace("Not","Pas"));
							span.html(span.html().replace("Click Here","Cliquez ici"));
						}
					});
				} else if (pageLang == 'ja') {
					// Encoded the japanese to solve garbling when uploading to pardot.
					// 私は、ThoughtSpotまたはそのパートナーが、マーケティング目的のため電話やEメールで連絡を取ることに同意します。私は、当該マーケティング情報の受け取りをいつでも停止できることを理解しています。
					GDPRMessage = decodeURI("%E7%A7%81%E3%81%AF%E3%80%81ThoughtSpot%E3%81%BE%E3%81%9F%E3%81%AF%E3%81%9D%E3%81%AE%E3%83%91%E3%83%BC%E3%83%88%E3%83%8A%E3%83%BC%E3%81%8C%E3%80%81%E3%83%9E%E3%83%BC%E3%82%B1%E3%83%86%E3%82%A3%E3%83%B3%E3%82%B0%E7%9B%AE%E7%9A%84%E3%81%AE%E3%81%9F%E3%82%81%E9%9B%BB%E8%A9%B1%E3%82%84E%E3%83%A1%E3%83%BC%E3%83%AB%E3%81%A7%E9%80%A3%E7%B5%A1%E3%82%92%E5%8F%96%E3%82%8B%E3%81%93%E3%81%A8%E3%81%AB%E5%90%8C%E6%84%8F%E3%81%97%E3%81%BE%E3%81%99%E3%80%82%E7%A7%81%E3%81%AF%E3%80%81%E5%BD%93%E8%A9%B2%E3%83%9E%E3%83%BC%E3%82%B1%E3%83%86%E3%82%A3%E3%83%B3%E3%82%B0%E6%83%85%E5%A0%B1%E3%81%AE%E5%8F%97%E3%81%91%E5%8F%96%E3%82%8A%E3%82%92%E3%81%84%E3%81%A4%E3%81%A7%E3%82%82%E5%81%9C%E6%AD%A2%E3%81%A7%E3%81%8D%E3%82%8B%E3%81%93%E3%81%A8%E3%82%92%E7%90%86%E8%A7%A3%E3%81%97%E3%81%A6%E3%81%84%E3%81%BE%E3%81%99%E3%80%82")

					$(document).ready(function() {
						var span = $('.email span.description').first();
						if (span.length) {
							span.html(span.html().replace("Not",""));
							span.html(span.html().replace("?",decodeURI("%20%E3%81%A7%E3%81%AA%E3%81%84%E5%A0%B4%E5%90%88%E3%81%AF%E3%80%81"))); // でない場合は、
							span.html(span.html().replace("Click Here",decodeURI("%E3%81%93%E3%81%A1%E3%82%89"))); // こちら
							span.html(span.html().replace(".",decodeURI("%E3%81%93%E3%81%A1%E3%82%89%E3%82%92%E3%82%AF%E3%83%AA%E3%83%83%E3%82%AF"))); // こちらをクリック
							try {
								var textNode = span.contents()[0]
								var stringArray = textNode.textContent.split(' ');
								stringArray = stringArray.filter(item => item);
								var nameArray = stringArray.slice(0, -1)
								var postNameString = stringArray.slice(-1)[0]
								var nameString = nameArray.reverse().join(' ')
								textNode.textContent = nameString + ' ' + postNameString
							} catch(err) {}
						}
					});
				}

				function markValidation(elem, validationStatus) {

					let error = elem.next('.error')
					let validationMessage = null

					if (validationStatus && validationStatus[0])
						validationStatus = validationStatus[0]

					if (validationStatus == "can't be blank") {
						validationMessage = 'This field is required'
						if (pageLang == 'ja') {
							validationMessage = decodeURI("%E3%81%93%E3%81%AE%E9%A0%85%E7%9B%AE%E3%81%AF%E5%BF%85%E9%A0%88%E3%81%A7%E3%81%99%E3%80%82") // この項目は必須です。
						} else if (pageLang == 'de') {
							validationMessage = 'Dieses Feld wird benötigt'
						} else if (pageLang == 'fr') {
							validationMessage = 'Ce champ est requis'
						}
					}

					if (validationStatus == "Email address required") {
						validationMessage = validationStatus
						if (pageLang == 'ja') {
							validationMessage = decodeURI("%E3%83%A1%E3%83%BC%E3%83%AB%E3%82%A2%E3%83%89%E3%83%AC%E3%82%B9%E3%81%8C%E5%BF%85%E8%A6%81%E3%81%A7%E3%81%99") // メールアドレスが必要です
						} else if (pageLang == 'de') {
							validationMessage = 'Email addresse wird benötigt'
						} else if (pageLang == 'fr') {
							validationMessage = 'Adresse e-mail requise'
						}
					}

					if (validationStatus == "Business emails only") {
						validationMessage = validationStatus
						if (pageLang == 'ja') {
							validationMessage = decodeURI("%E5%8B%A4%E5%8B%99%E5%85%88%E3%81%AE%E3%83%A1%E3%83%BC%E3%83%AB%E3%82%A2%E3%83%89%E3%83%AC%E3%82%B9%E3%81%AE%E3%81%BF") // 勤務先のメールアドレスのみ
						} else if (pageLang == 'de') {
							validationMessage = 'Nur Firmenadressen bitte'
						} else if (pageLang == 'fr') {
							validationMessage = 'Email professionnel (uniquement)'
						}
					}

					if (elem.is('select') && elem.val() == elem.find('option:first-child').attr('value'))
						validationStatus = true

					if (validationStatus && elem.is(':visible')) {
						if (!error.length)
							error = $('<p class="error">' + validationMessage + '</p>')

						error.html(validationMessage)
						error.show()
						error.insertAfter(elem)
					} else {
						error.hide()
					}

					if (form.find('.error:visible').length) {
						submitButton.attr('disabled', true)
					} else {
						enableSubmitButton()
					}
				}

				function checkAllValidation() {
					for (var i in validatedFields) {
						if (validatedFields[i] && validatedFields[i].trigger) {
							validatedFields[i].trigger('blur')
						}
					}
				}

				const fields = {
					first_name: '#First_Namepi_First_Name',
					last_name: '#Last_Namepi_Last_Name',
					email: '#Emailpi_Email',
					companyNumber: 'Work Phone',
					jobRole: 'Job Role',
					country: 'Select Country',
					state: 'Select State',
					industry: 'Industry',
					importTitle: '.Import_Title'
				}

				if (pageLang == 'ja') {
					fields.companyNumber = decodeURI("%E4%BC%9A%E7%A4%BE%E9%9B%BB%E8%A9%B1") // 会社電話
					fields.jobRole = decodeURI("%E5%BD%B9%E8%81%B7") // 役職
					fields.country = decodeURI("%E5%9B%BD%E3%82%92%E9%81%B8%E6%8A%9E") // 国を選択
					fields.state = decodeURI("%E5%B7%9E%E3%82%92%E9%81%B8%E6%8A%9E") // 州を選択
					fields.industry = decodeURI("%E6%A5%AD%E7%95%8C") // 業界
					fields.importTitle = '.Import_Title'
				} else if (pageLang == 'de') {
					fields.salutation = 'Anrede'
					fields.companyNumber = 'Geschäftliche Telefonnummer'
					fields.jobRole = 'Position auswählen'
					fields.country = 'Land auswählen'
					fields.state = 'Select State'
					fields.industry = 'Branche auswählen'
				} else if (pageLang == 'fr') {
					fields.salutation = 'Civilité'
					fields.companyNumber = 'Tél.'
					fields.jobRole = "Fonction"
					fields.country = 'Pays'
					fields.state = "État"
					fields.industry = 'Industrie'
				}

				const salutationField = form.find('label:contains("' + fields.salutation + '")').next()
				const salutationFieldWrapper = salutationField.parent('.form-field')

				const firstNameField = form.find(fields.first_name)
				const firstNameFieldWrapper = firstNameField.parent('.form-field')

				const lastNameField = form.find(fields.last_name)
				const lastNameFieldWrapper = lastNameField.parent('.form-field')

				const emailField = form.find(fields.email).length ? form.find(fields.email) : form.find('.form-field.email input')
				const emailFieldWrapper = emailField.parent('.form-field')

				const companyNumberField = form.find('label:contains("' + fields.companyNumber + '")').next()
				const companyNumberFieldWrapper = companyNumberField.parent('.form-field')

				const jobRoleField = form.find('label:contains("' + fields.jobRole + '")').next()
				const jobRoleWrapper = jobRoleField.parent('.form-field')

				const countryField = form.find('label:contains("' + fields.country + '")').next()
				const countryFieldWrapper = countryField.parent('.form-field')

				const stateField = form.find('label:contains("' + fields.state + '")').next()
				const stateFieldWrapper = stateField.parent('.form-field')

				const industryField = form.find('label:contains("' + fields.industry + '")').next()
				const industryFieldWrapper = industryField.parent('.form-field')
				industryFieldWrapper.hide()

				const gdprField = form.find('.GDPR_Consent_Checkbox input, .GDPR_Express_Consent input')
				const gdprFieldWrapper = form.find('.GDPR_Consent_Checkbox')

				const importTitleField = form.find(fields.importTitle + ' input')
				const importTitleFieldWrapper = importTitleField.parent('.form-field')

				const CloudExpressAdminAccessCheckbox = form.find('.CloudExpress_Has_AWS_Access input')
				const CloudExpressDataAccessCheckbox = form.find('.CloudExpress_Has_Data_Access input')

				const saasTrialDataAccessField = form.find('.SaaS_Trial_Data_Access select')

				const successRedirect = form.find('.success_redirect input')

				// console.log([firstNameField, lastNameField, emailField, companyNumberField, jobRoleField, countryField, stateField, industryField, gdprField, importTitleField])

				const validatedFields = [firstNameField, lastNameField, emailField, companyField, companyNumberField, jobRoleField, countryField, stateField, importTitleField]

				if (window.cloudExpress) {
					validatedFields.push(CloudExpressAdminAccessCheckbox)
					validatedFields.push(CloudExpressDataAccessCheckbox)
				}

				if (saasTrialDataAccessField.length) {
					if (saasTrialDataAccessField.find('option').length)
						saasTrialDataAccessField.find('option').eq(0).val('')
					validatedFields.push(saasTrialDataAccessField)
				}


				if (salutationField.length)
					validatedFields.push(salutationField)

				let dbSources = []
				let dbCompany = []

				function validateEmail(email) {
					if (validate.single(email, {presence: true, email: true})) {
						console.log('invalid email')
						return ['Email address required'];
					}

					if (window.businessEmailsOnly) {
						let provider = email.split('@')[1]
						if (blackList.indexOf(provider) > -1 || personalEmails.indexOf(provider) > -1) {
							console.log('Personal email')
							return ['Business emails only'];
						}
					}

					return null;
				}

				var keyupTimeout = null;
				for (var i in validatedFields) {
					const input = $(validatedFields[i]);

					if (!input.length)
						continue

					if (input.is('[type="checkbox"]') && (input.prop('required') || input.parents('.required').length)) {
						input.blur(function() {
							markValidation(input, !input.is(':checked'))
						}).click(function() {
							markValidation(input, !input.is(':checked'))
						})
					} else {
						input.blur(function(e) {
							if (this == emailField[0]) {
								markValidation(input, validateEmail(input.val()))
							} else {
								markValidation(input, validate.single(input.val(), {presence: {allowEmpty: false}}))
							}
						})

						if (input.is('input')) {
							input.keyup(function(e) {
								// e.key == 'Delete' || e.key == 'Backspace' ||
								if (e.key == 'Tab')
									return

								if (keyupTimeout)
									clearTimeout(keyupTimeout);

								keyupTimeout = setTimeout(function() {
									if (input[0] == emailField[0]) {
										markValidation(input, validateEmail(input.val()))
									} else {
										markValidation(input, validate.single(input.val(), {presence: {allowEmpty: false}}))
									}
								}, 501);
							})
						}

						input.change(function(e) {
							if (this == countryField[0])
								$('#invalid_country').hide()

							if (this == emailField[0]) {
								markValidation(input, validateEmail(input.val()))
							} else {
								markValidation(input, validate.single(input.val(), {presence: {allowEmpty: false}}))
							}
						})
					}
				}

				// When they change the email hide this error. It will be shown again if they didn't follow instructions
				emailField.on('keyup, blur', function(e) {
					setTimeout(function() {
						$('#invalid_email').hide()
						$('#competitor_email').hide()
						$('#user_already_active').hide()
						$('#user_authenticate_email').hide()
						enableSubmitButton()
					}, 500)
				})

				stateField.find('option').each(function() {
					const option = $(this)
					if (canadianProvinceNames.indexOf(option.text()) > -1) {
						canadianProvinces = canadianProvinces.add(option.remove())
					} else if (pageLang == 'ja') {
					} else if (option.text()) {
						usaStates = usaStates.add(option.remove())
					}
				})

				// window.addEventListener('load', function() {
				// 	window.ClearbitForPardot.onResult(function(result) {
				// 		console.log(result)
				// 	})
				// })

				form.hide()

				const clearbitInterval = setInterval(function() {
					if (window.ClearbitForPardot) {
						clearInterval(clearbitInterval)

						const originalOnResult = window.ClearbitForPardot.onResult

						setTimeout(function() {
							form.show()
						}, 100)

						window.ClearbitForPardot.onResult = function(email, attributes) {
							form.show()
							originalOnResult(email, attributes)
							checkGDPR(attributes.Clearbit_HQ_Country, false)
						}
						window.ClearbitForPardot.initialize()
					}
				}, 100)

				setTimeout(function() {
					form.show()
				}, 500)

				window.db_afterParse_hook = function(data, source) {
					form.show()

					console.log('AFTERPARSE HOOK!', {data: data}, {source: source});

					dbSources.push(source)
					dbCompany.push(data)

					afterParseValue = data.company_name
					afterParseCountry = data.country_name

					checkGDPR(data.country_name, 'ip')

					if (dbSources.indexOf('domain') > -1 || dbSources.indexOf('company') > -1 ) {
						hideCountry()
					} else if (source != 'ip') {
						showCountry()
					}
				}

				window.db_noSourceMatch = function() {
					console.log('NO SOURCE MATCH');
					showCountry()
					countrySelected()
				};

				// gdprField.change(function() {
				// 	console.log($(this).val())
				// })

				countryField.change(function() {
					countrySelected()
				})

				// companyField.blur(function() {
					// setTimeout(function() {
					// 	if (afterParseValue != companyField.val()) {
					// 		console.log('companyFieldBlur', afterParseValue != companyField.val(), afterParseValue, companyField.val())
					// 		afterParseValue = companyField.val()
					// 		clearDB();
					// 		showCountry();
					// 		countrySelected();
					// 	}
					// 	// console.log('companyField.blur', companyField.val(), afterParseValue)
					// }, 300)
				// })

				const clearDB = function() {
					form.find('.form-field').each(function() {
						if ($(this).attr('class').includes('DB_')) {
							$(this).find('input[type="hidden"]').val('')
						}
					})
				}

				const hideCountry = function() {
					console.log('hideCountry')
					countryFieldWrapper.hide()
					stateFieldWrapper.hide()
					resizeIframe()
				}

				const showCountry = function() {
					console.log('showCountry')
					if (!countryField.is(':visible'))
						countryField.val(countryField.find('option').eq(0).val())
					countryFieldWrapper.show()
					resizeIframe()
				}

				const countrySelected = function() {
					const country = countryField.find('option:selected').text()

					if (country == 'United States' || country == 'USA') {
						$(canadianProvinces).remove()
						stateField.append(usaStates)
						stateFieldWrapper.show()
						stateField.find('option').eq(0).text('Select State...')
					} else if (country == 'Canada' || country == 'Kanada') {
						$(usaStates).remove()
						stateField.append(canadianProvinces)
						stateFieldWrapper.show()
						stateField.find('option').eq(0).text('Select Province...')
					} else if (pageLang == 'ja' && country == decodeURI("%E6%97%A5%E6%9C%AC")) { // 日本
						stateFieldWrapper.show()
					} else {
						stateFieldWrapper.val('').hide()
					}

					checkGDPR(country)
					resizeIframe()
				}

				const checkGDPR = function(country, ip) {
					// console.log('checkGDPR', country, countryName.indexOf(country), ip)
					if (countryName.indexOf(country) > -1 || gdprIPMatch) {
						if (ip)
							gdprIPMatch = true

						showGDPR()

						if (country == 'Japan')
							gdprField.prop("checked", true)

					} else {
						hideGDPR()
					}

					if (country == 'India' && companyNumberField && companyNumberField.length)
						companyNumberField.attr('placeholder', 'Mobile Phone')
				}

				const showGDPR = function() {
					// console.log('showGDPR')
					if (gdprFieldWrapper.length) {
						gdprFieldWrapper.show()
						// industryFieldWrapper.show()
						submitButton.attr('disabled', true)
						resizeIframe()
					}
				}

				const hideGDPR = function() {
					// console.log('hideGDPR')
					gdprFieldWrapper.hide()
					// industryFieldWrapper.hide()
					enableSubmitButton()
					resizeIframe()
				}

				const redirect = function(url) {
					window.parent.postMessage({
						event_id: 'redirect',
						data: {
							url: url
						}
					}, '*');
				}

				const checkForSubmitError = function() {
					let checkInterval = setInterval(function() {
						if (submitButton.attr('disabled')) {
							submitP.removeClass('show_loading')
							clearInterval(checkInterval)
						}
					}, 500)
				}

				const resizeIframe = function() {
					// console.log('resizeIframe', $('form').height())
					window.parent.postMessage({
						event_id: 'resize',
						data: {
							height: $('form').height(),
							url: window.location.href
						}
					}, '*');
				}

				function addPlaceholder() {
					const input = $(this)
					const label = input.prev()

					if (input.is('select')) {
						input.find('option:first-child').text(label.text() + '...')
					} else {
						input.attr('placeholder', label.text())
					}
				}

				function enableSubmitButton() {
					if (gdprField.is(':visible') && !gdprField.is(':checked')) {
						return
					}

					if (form.find('.error:visible').length) {
						return
					}

					submitButton.attr('disabled', false)
				}

				function updateGoogleSpreadsheet(done) {
					try {
						if (asyncError)
							return done()


						var spreadsheetData = [];
						window.spreadsheetFields.forEach(function(selector) {
							if (selector.indexOf('select') > -1) {
								spreadsheetData.push(encodeURIComponent($(selector).find('option:selected').text().replaceAll(',', '')));
							} else {
								spreadsheetData.push(encodeURIComponent($(selector).val().replaceAll(',', '')));
							}
						});

						datadogLogs.logger.info('updateGoogleSpreadsheet - Running', spreadsheetData)
						console.log('updateGoogleSpreadsheet - Running')

						function reqListener () {
							// console.log('response', this.responseText)
							if (this.responseText != 'success') {
								console.log('pardotUpdateGoogleSheet - Failed', {status: this.status, resp: this.responseText})
								var3input.val(var3input.val() + ' pardotUpdateGoogleSheet - Failed: ' + JSON.stringify({status: this.status, resp: this.responseText}))
								datadogLogs.logger.info('pardotUpdateGoogleSheet - Failed', {data: spreadsheetData, status: this.status, resp: this.responseText})
								// Disable this for now.
								// $('#failed_spreadsheet_update').show()
								done()
							} else {
								datadogLogs.logger.info('pardotUpdateGoogleSheet - Success', {data: spreadsheetData, status: this.status, resp: this.responseText})
							}
							return done()
						}

						var http_request = new XMLHttpRequest();

						http_request.addEventListener("load", reqListener);
						http_request.addEventListener("error", function(e) {
							datadogLogs.logger.info('pardotUpdateGoogleSheet - http_request Error', {data: e})
							var3input.val(var3input.val() + ' pardotUpdateGoogleSheet - http_request Error: ' + JSON.stringify({data: e}))
							console.log('pardotUpdateGoogleSheet - http_request failed')
							// $('#failed_spreadsheet_update').show()
							done()
						});

						// http_request.open("GET", 'https://thoughtspot.ngrok.io/pardot/update_pardot_spreadsheet?values=' + spreadsheetData.join(',') + '&sheet=' + window.updateSpreadsheet);
						http_request.open("GET", 'https://www.thoughtspot.com/pardot/update_pardot_spreadsheet?values=' + spreadsheetData.join(',') + '&sheet=' + window.updateSpreadsheet);
						http_request.withCredentials = false;
						http_request.setRequestHeader("Content-Type", "application/json");
						http_request.send({});
					} catch (e) {
						// $('#failed_spreadsheet_update').show();
						done()
					}
				}

				function createCloudExpressKey(done) {
					console.log('createCloudExpressKey')
					if (asyncError)
						return done()

					console.log('createCloudExpressKey - Running')

					const email = emailField.val()

					var http_request = new XMLHttpRequest();

					function reqListener () {
						if (this.responseText) {
							const parsed = JSON.parse(this.responseText)
							console.log('api response', parsed)
							return done(parsed);
						}
						return done(false)
					}

					const lang = window.location.search.match(/lang=(.+)&/)
					let langParam = ''
					if (lang && lang[1])
						langParam = '&lang=' + lang[1]
					if (!langParam && pageLang)
						langParam = '&lang=' + pageLang

					http_request.addEventListener("load", reqListener);
					http_request.addEventListener("error", function() {
						done(false)
					});
					// http_request.open("GET", `https://cloudexp-thoughtspot.pantheonsite.io/pardot/generate-license-key?email=${email}`, true);
					// http_request.open("GET", `https://user-api-thoughtspot.pantheonsite.io/pardot/generate-license-key?email=${email}${langParam}`, true);
					http_request.open("GET", `https://www.thoughtspot.com/pardot/generate-license-key?email=${email}${langParam}`, true);
					http_request.withCredentials = false;
					http_request.setRequestHeader("Content-Type", "application/json");
					http_request.send({});
				}

				inputFields.each(addPlaceholder)
				selectFields.each(addPlaceholder)

				gdprFieldWrapper.hide()
				industryFieldWrapper.hide()
				countryFieldWrapper.hide()
				stateFieldWrapper.hide()

				gdprField.on('change', function() {
					if (gdprField.is(':checked')) {
						enableSubmitButton()
					} else {
						submitButton.attr('disabled', true)
					}
				})


				let skipSubmitLogic = false
				window.formWillSubmit = false
				// let createdCloudExpressKeySuccess = false
				// let createdSpreadsheetSuccess = false
				let asyncError = false

				form.on('submit', function() {
					if (skipSubmitLogic) {
						var3input.val(var3input.val() + ' Submit: ' + JSON.stringify({location: window.location.href, asyncError: asyncError, spreadsheet: window.updateSpreadsheet}))
						datadogLogs.logger.info('Pardot Form Submitting', {location: window.location.href, asyncError: asyncError, spreadsheet: window.updateSpreadsheet, formData: $('.pardot-form').serializeArray()})
						skipSubmitLogic = false

						// try {
						// 	if (window.location.search.indexOf('metadata_cid') > -1) {
						// 		const formValues = Object.fromEntries(new FormData(form[0]));
						// 		console.log('Test Metadata Values', formValues)
						// 		window.Metadata.siteScript.sendData(formValues)
						// 	}
						// } catch (e) {
						// 	console.log('submitToPardot Metadata fail', e)
						// }

						formSubmittedMixpanelMessage('Pardot Form Submitted')
						window.formWillSubmit = true
						return true
					}

					formSubmittedMixpanelMessage('Trial Form Submit Button Clicked')

					checkAllValidation()

					if (submitButton.attr('disabled')) {
						console.log('Aborting submit. Invalid values.')
						return false
					}

					submitP.addClass('show_loading')

					let asyncCount = 0
					let asyncCompletedTarget = 0

					function updatedSpreadsheet() {
						if (!asyncError) {
							if ($('#failed_spreadsheet_update').is(':visible')) {
								submitP.removeClass('show_loading')
								submitButton.attr('disabled', true)
								asyncError = true
								setTimeout(function() {
									$('#failed_spreadsheet_update').hide();
									enableSubmitButton();
								}, 4000);
							}
						}
						asyncCount += 1
						asyncCompleted()
					}

					function createdCloudExpressKey(created) {
						if (!asyncError) {
							if (created) {
								form.find('.CloudExpress_License input').val(created.key)
								form.find('.CloudExpress_License_URL input').val(created.key_link)
								form.find('.UUID input').val(created.uuid)
							} else {
								$('#failed_key_creation').show()
								asyncError = true
								setTimeout(function() {
									$('#failed_key_creation').fadeOut()
								}, 4000)
							}
						}
						asyncCount += 1
						asyncCompleted()
					}

					function asyncCompleted() {
						if (asyncCount >= asyncCompletedTarget) {


							if ($('.error:visible').length || asyncError) {
								submitP.removeClass('show_loading')
								asyncCount = 0
								asyncError = false
								console.log('error')
							} else {
								asyncCount = 0
								asyncError = false
								skipSubmitLogic = true
								form.submit()
							}
						}
					}

					if (window.updateSpreadsheet && window.cloudExpress) {
						asyncCompletedTarget = 2
						updateGoogleSpreadsheet(function() {
							updatedSpreadsheet()
							createCloudExpressKey(createdCloudExpressKey)
						})
					} else if (window.updateSpreadsheet) {
						asyncCompletedTarget = 1
						updateGoogleSpreadsheet(updatedSpreadsheet)
					} else if (window.cloudExpress) {
						asyncCompletedTarget = 1
						createCloudExpressKey(createdCloudExpressKey)
					} else {
						skipSubmitLogic = true
						form.submit()
					}

					// This is so the submit spinner goes away if pardot stops the form submit.
					checkForSubmitError()
					return false
				})

				$(window).on('load', function() {
					setTimeout(function() {
						resizeIframe()
					}, 1000)
				});

				setInterval(resizeIframe, 1000)

				// Test
				// $(window).on('keyup', function(e) {
				// 	if (e.key == ',') {
				// 		db_noSourceMatch()
				// 	} else if (e.key == '.') {
				// 		db_afterParse_hook({country_name: 'Ireland'}, 'company')
				// 	}
				// });

				let matched = document.location.search.match(/style=(\w+)/)
				if (matched && matched[1])
					form.addClass('style_' + matched[1])

				form.addClass('prepared')

				function geolocate() {
					var http_request = new XMLHttpRequest();
					function reqListener () {
						console.log('geolocate', this.responseText)
						if (this.responseText) {
							let parsed = {}
							try {
								parsed = JSON.parse(this.responseText)
							} catch (e) {}

							if (parsed.state)
								form.find('.Inferred_State input').val(parsed.state)
							if (parsed.country) {
								form.find('.Inferred_Country input').val(parsed.country)
								if (!ipCountry)
									ipCountry = parsed.country
							}

							if (parsed.zip)
								form.find('.Inferred_Zip_Code input').val(parsed.zip)

							if (parsed.city)
								form.find('.Inferred_City input').val(parsed.city)

							checkGDPR(parsed.country, 'ip')
						}
					}

					http_request.addEventListener("load", reqListener);
					http_request.open("GET", "https://prod.tsfunctions.net/geolocate");
					http_request.withCredentials = false;
					http_request.setRequestHeader("Content-Type", "application/json");
					http_request.send({});
				}

				geolocate()

				submitButton.attr('disabled', true)

				$(window).on("message", function(e) {
					const message = e.originalEvent.data

					// console.log('iframe received message', message)
					if (message && message.event_id == 'passthrough_arg' && message.data.argname && message.data.argvalue) {
						$('.' + message.data.argname + ' input').val(message.data.argvalue)
						// console.log('set arg', '.' + message.data.argname + ' input', $('.' + message.data.argname + ' input'));
					}

					if (message && message.event_id == 'geolocated') {
						if (message.data) {
							const parsed = JSON.parse(message.data)
							if (parsed.state)
								form.find('.Inferred_State input').val(parsed.state)
							if (parsed.country)
								form.find('.Inferred_Country input').val(parsed.country)
							if (parsed.city)
								form.find('.Inferred_City input').val(parsed.city)
						}
					}
				});

				$('.GDPR_Consent_Checkbox').each(function() {
					$(this).find('label').text(GDPRMessage);
				});

				// I'm mapping placeholder back onto label since the code was originally written to customize placeholder because that's what we were displaying.
				inputFields.each(function() {
					$(this).prev().html($(this).attr('placeholder'))
					if ($(this).val())
						$(this).prev().addClass('populated')
				})

				inputFields.on('focus', function() {
					$(this).prev().addClass('focused')
				}).on('blur', function() {
					$(this).prev().removeClass('focused')
					if ($(this).val()) {
						$(this).prev().addClass('populated')
					} else {
						$(this).prev().removeClass('populated')
					}
				})

				selectFields.on('change', function() {
					if ($(this).val() && $(this).val() != $(this).find('option').eq(0).attr('value')) {
						$(this).addClass('populated').prev().addClass('populated')
					} else {
						$(this).removeClass('populated').prev().removeClass('populated')
					}
				}).trigger('change')

				// #dev autofill
				// console.log('autofill')
				// firstNameField.val('Joren')
				// lastNameField.val('Mathews')
				// emailField.val('loremipsum99@mailinator.com')
				// companyField.val('3fo')
				// companyNumberField.val('4085883222')
				// jobRoleField.val('693143') // Developer
				// saasTrialRoleField.val('693773') // Data analyst
				// // countryField.val('693151') // USA
				// // stateField.val('693667') // Arizona
				// saasTermsAcceptedCheckbox.prop('checked', 'checked')

			});

		})();
	}
}