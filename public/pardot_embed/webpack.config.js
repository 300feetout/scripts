const webpack = require('webpack')
const path = require('path')
const glob = require('glob')
const LodashModuleReplacementPlugin = require('lodash-webpack-plugin') // Tree shakes Lodash to minimize file sizes
const CssMinimizerPlugin = require("css-minimizer-webpack-plugin")

const TerserPlugin = require("terser-webpack-plugin")



const devMode = process.env.NODE_ENV !== 'production'

var config = {
	devtool: "source-map",
	mode: 'production',	
	node: {
		global: false
	},
	resolve: {
		fallback: {
			"fs": false,
			"util": false,
			"path": false,
			"crypto": false,
			"zlib": false,
			"stream": false,
			"buffer": false,
			"https": false,
			"http": false,
			"url": false,
			"querystring": false,
			"os": false,
			"esbuild": false,
			"assert": false,
			"constants": false,
			"child_process": false,
			"worker_threads": false,
			"inspector": false,
			"@swc/core": false,
			"vm": false
		},
	},
	module: {
		rules: [
			{
				test: /\.m?js$/,
				exclude: /(node_modules|bower_components)/,
				use: {
					loader: 'babel-loader',
					options: {
						plugins: ['lodash'],
						presets: ['@babel/preset-env']
					}
				}
			},
			{
				test: /\.styl$/,
				use: [
					{
						loader: "style-loader" // creates style nodes from JS strings
					},
					{
						loader: "css-loader" // translates CSS into CommonJS
					},
					{
						loader: "stylus-loader", // compiles Stylus to CSS
						options: {
							stylusOptions: {
								use: [
									require('nib')(),
									require('axis')(),
									require('rupture')(),
									require('jeet')()
								]
							}
						}
					},
				]
			},
		]
	},
	plugins: [
		new LodashModuleReplacementPlugin,
		new webpack.DefinePlugin({
			global: 'window' // Placeholder for global used in any node_modules
		})
	],
	optimization: {
		minimize: true,
		minimizer: [
			new TerserPlugin()
		]
	}
};

var pardotEmbed = Object.assign({}, config, {
	name: 'common',
	entry: './src/pardot_embed.js',
	output: {
		path: path.resolve(__dirname, 'dist/'),
		filename: 'pardot_embed.bundled.js'
	},
});

var pardotEmbedFreeTrial = Object.assign({}, config, {
	name: 'common',
	entry: './src/pardot_embed_free_trial.js',
	output: {
		path: path.resolve(__dirname, 'dist/'),
		filename: 'pardot_embed_free_trial.bundled.js'
	},
});

module.exports = [
	pardotEmbed, pardotEmbedFreeTrial
];