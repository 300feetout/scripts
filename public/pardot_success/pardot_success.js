const pardotSuccessRedirect = window.pardotSuccessRedirect = function(url) {
	// Not sure why this ampersand is being encoded, but I'm just gonna fix it.
	url = url.replace('&amp;', '&')

	console.log('REDIRECT', url)

	const whitelist = ['thoughtspot.com','go.thoughtspot','events.thoughtspot','community.thoughtspot','developers.thoughtspot','groups.thoughtspot','training.thoughtspot','docs.thoughtspot','thoughtspot.cloud','thoughtspotstaging.cloud','thoughtspotdev.cloud']

	let validRedirect = false
	whitelist.map((whitelistUrlRaw) => {
		if (url.indexOf(whitelistUrlRaw) > -1) {
			validRedirect = true
		}
	})

	if (!validRedirect) {
		url = 'https://www.thoughtspot.com'
	}

	try {
		if (window.parent.location.hostname == 'go.thoughtspot.com') {
			console.log('redirect')
			window.parent.location = url;
			return;
		}
	} catch (e) {

	}

	if (window.parent) {
		console.log('postMessage redirect')
		window.parent.postMessage({
			event_id: 'redirect',
			data: {
				url: url,
			}
		}, '*');

	} else {
		window.location = url
	}
}

setTimeout(function() {
	function getUrlParameter(parameterName) { 
		var queryString = document.URL; 
		var parameterName = parameterName + "="; 
		if (queryString.length > 0) {
			var begin = queryString.indexOf(parameterName);
			if (begin != -1) {
				begin += parameterName.length;
				var end = queryString.indexOf( "&" , begin);
				if (end == -1) {
					end = queryString.length
				}
				return unescape(queryString.substring(begin, end));
			}
		}
		return null;
	}

	var Url = getUrlParameter("success_redirect");
	// console.log('window', window)
	// console.log('SUCCESS!!!', Url, success_redirect, window.parent)

	console.log('pardot_success', window.location)

	if (window.skipSuccess) {
		console.log('skipSuccess', window.skipSuccess)
		return;
	}

	console.log('pardot success redirect', pardotSuccessRedirect)

	if (Url != null) {
		pardotSuccessRedirect(Url)
	} else {
		pardotSuccessRedirect(success_redirect)
	}
}, 200);