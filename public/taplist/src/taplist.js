require('./css/taplist.styl');

import "babel-polyfill"
import {Buffer} from 'buffer'

(function() {
	let taplistHtml = ''
	let taplistContainer = null

	async function apiRequest(url) {
		const authString = btoa('rex@300feetout.com:aShkzncr3ikmCsGxgMXz')

		const response = await fetch(url, {
			headers: {
				'Authorization': `Basic ${authString}`
			}
		})
	
		return await response.json()
	}

	async function awaitRender(promise, target) {
		promise.then((value) => {
			const interval = setInterval(() => {
				const targetContainer = taplistContainer.querySelector('#' + target)
				if (targetContainer) {
					targetContainer.innerHTML = value
					clearInterval(interval)
				}
			}, 500)
		})
	}

	async function outputContainerSizes(item) {
		const containerSizes = (await apiRequest(`https://business.untappd.com/api/v1/items/${item.id}/containers`)).containers

		let html = `<span class="text-fit text-overflow" data-max-font="35" data-min-font="10" style="font-size: ${containerSizes.length > 1 ? 16 : 16}px;">`

		for (const container of containerSizes) {
			html += `<span class="container-item">
				<span class="name">${container.container_size.name}</span>
			</span>`
		}

		// <span class="container-calories">[${container.calories} calories</span>
		// 	<span class="price">
		// 		<span class="currency-symbol">$</span>${container.price}]
		// 	</span>
				
		html += '</span>'

		return html
	}

	async function outputSection(section) {

		const sectionItems = await apiRequest(`https://business.untappd.com/api/v1/sections/${section.id}/items`)

		if (!sectionItems.items.length)
			return ''

		let sectionHtml = `
			<div class="taplist_section">
				<div class="section-title">${section.name}</div>`

			for (const item of sectionItems.items) {
				sectionHtml += `<div class="item" id="item-${item.id}">
					<div class="info">
						<div class="basic">
							<span class="text-fit text-overflow" data-max-font="40" data-min-font="10">
								 <span class="brewery">${item.brewery}</span>
								 <span class="name">${item.name}</span>
							</span>
						</div>
						<div class="detail">
							<span class="text-fit text-overflow" data-max-font="40" data-min-font="10">
								<span class="style">${item.style}</span>
								<span class="separator"></span>
								<span class="abv">${item.abv}%</span>
							</span>
						</div>
					</div>
					<div class="containers" id="containers-${item.id}"></div>
				</div>`

				awaitRender(outputContainerSizes(item), `containers-${item.id}`)
			}

		sectionHtml += `</div>`

		return sectionHtml
	}

	async function renderTaplist(sections) {

		const promises = []
		for (const section of sections) {
			promises.push(outputSection(section))
		}

		Promise.all(promises).then((values) => {
			taplistHtml += values.join('')
			taplistContainer.innerHTML = taplistHtml
		})

	}

	async function initTaplist() {
		const pageWrapper = document.querySelector('.entry-content, .page__content')
		taplistContainer = document.createElement('div')
		pageWrapper.appendChild(taplistContainer)
		taplistContainer.classList.add('taplist')
		taplistContainer.innerHTML = '<div class="items"><div class="column"></div></div>'
		taplistContainer = taplistContainer.querySelector('.column')

		const response = await apiRequest('https://business.untappd.com/api/v1/menus/91304/sections?include_on_deck_section=true')

		renderTaplist(response.sections)
	}

	window.addEventListener('load', initTaplist)
})()